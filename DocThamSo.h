/* Đọc Tham Số */

#pragma mark Đọc Tham Số Dòng Lệnh

/* đọc tham số phim trường */
void docThamSoPhimTruong( int argc, char **argv, unsigned int *soPhimTruong );

/* đọc tham số hoạt hình */
void docThamSoHoatHinh( int argc, char **argv, unsigned int *soHoatHinhDau, unsigned int *soHoatHinhCuoi );

/* đọc tham kích thước ảnh */
void docThamCoKich( int argc, char **argv, unsigned int *beRong, unsigned int *beCao, float *coThuocDiemAnh );

/* đọc tham số xài ô */
void docThamXaiO( int argc, char **argv, unsigned char *xaiO, unsigned short *beRongO, unsigned short *beCaoO, unsigned char *tinhTiep );
