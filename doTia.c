//  Ví dụ phương pháp kết xuất dò tia đơn giản
//  Phiên Bản 4.5
//  Phát hành 2562/01/27
//  Hệ tọa độ giống OpenGL (+y là hướng lên)
//  Khởi đầu 2557/12/18

//  Sai lầm Segmentation fault: 11 thường nghĩa là có hai vật thể cùng tâm và không thể xây cây thần bậc

//  Biên dịch cho gcc: gcc -lm -lz doTia.c -o <tên chương trình>
//  Biên dịch cho clang: clang -lm -lz doTia.c -o <tên chương trình>

//  Lệnh dạng: <sỗ phim trường> <số hoạt hình đầu> <số hoạt hình cuối> <beRồng ảnh> <bề cao ảnh> <cỡ kích điểm ảnh>
//  ---- Cho phối cảnh
//  Lệnh dạng ví dụ: <tên chương trình biên dịch> 0  0 1520   600  300   0.01
//  Lệnh dạng ví dụ: <tên chương trình biên dịch> 0  0 1520  8192 4096  0.00075 <---- 8K  (8192 x 4320)
//  Lệnh dạng ví dụ: <tên chương trình biên dịch> 0  0 1520  4096 2048  0.0015  <---- 4K  (4096 x 2160)
//  Lệnh dạng ví dụ: <tên chương trình biên dịch> 0  0 1520  2048 1024  0.003  <---- 2K
//  Lệnh dạng ví dụ: <tên chương trình biên dịch> 0  0 1520  1536  768  0.004  <---- ~ Câu Truyện Đồ Chơi 1
//  Lệnh dạng ví dụ: <tên chương trình biên dịch> 0  0 1520  1024  512  0.006
//  Lệnh dạng ví dụ: <tên chương trình biên dịch> 0  0 1520   512  256  0.012
//  Lệnh dạng ví dụ: <tên chương trình biên dịch> 0  0 1520   256  128  0.024
//
//  Lệnh dạng ví dụ: <tên chương trình biên dịch> 0  0 1520  1024 512  0.006 o  <---- tính ảnh EXR ô
//  Lệnh dạng ví dụ: <tên chương trình biên dịch> 0  0 1520  1024 512  0.006 t  <---- tính ảnh EXR ô, mmà tính tiếp tục


//  • CHO MÁY PHIM 1 ĐƠN VỊ CHIẾU_TOAN_CANH ≈ 5/24 ĐƠN VỊ CHIẾU_PHỐI_CẢNH
//  • Đừng cho vật thể nào có bao bì bề nào = 0
//  - Hình Cầu
//  - Mặt Phẳng
//  - Hợp
//  - Hình Trụ
//  - Hình Nón
//  - Mặt Hyperbol
//  - Mặt Parabol
//  - Tứ Diện
//  - Bát Diện
//  - Thập Nhị Diện
//  - Kim Tư Tháp
//  - Dốc
//  - Nhị Thập Diện
//  - Sao Gai
//  - Mặt Sóng
//  - Hình Xuyến

//  - Vật Thể Bool
//  - Vật Thể Ghép

//  - Biến Hóa cho vật thể, không đặt vị trí hay phóng to ---> xài ma trận;
//       Lưu Ý: nên không xài cho phóng to lớn cho vật thể xa máy quay phim
//  - Họa tiết tô màu
//  - Phim Trường
//  - ba chiếu đồ: phối cảnh, cự tuyến, toàn cảnh
//  - phản xạ thuộc màu cho một họa tiết có thể có phản xạ không đều
//  - bóng tối trong, nhưng chỉ xài đục của vật thể đầu tiên tia bóng tối gặp

//  Hồ Nhựt Châu  su_huynh@yahoo.com

#include "Anh.h"
#include "ThongTinOChuNhat.h"
#include "Toan/Vecto.h"
#include "Toan/Tia.h"
#include "Toan/BienHoa.h"
#include "XemCat/XemCat.h"
#include "DocThamSo.h"
#include "PhimTruong/PhimTruong0.h"
#include "PhimTruong/PhimTruong1.h"
#include "PhimTruong/PhimTruong2.h"
#include "PhimTruong/PhimTruong3.h"
#include "PhimTruong/PhimTruongTN.h"
#include "ThongTinToMau.h"
#include "XemCat/TinhBaoBi.h"
#include "ToMau.h"
#include "LuuAnhEXR.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

// ---- tính hàng
void lapVongHang( PhimTruong *phimTruong, Anh *anh );

void veAnhChieuPhoiCanh( Anh *anh, PhimTruong *phimTruong, GiaoDiemBIH *mangGiaoDiem, unsigned int chiSoGiaoDiem );
void veAnhChieuTrucGiao( Anh *anh, PhimTruong *phimTruong, GiaoDiemBIH *mangGiaoDiem, unsigned int chiSoGiaoDiem );
void veAnhChieuToanCanh( Anh *anh, PhimTruong *phimTruong, GiaoDiemBIH *mangGiaoDiem, unsigned int chiSoGiaoDiem );

void veAnhChieuPhoiCanh_O( Anh *anh, PhimTruong *phimTruong, GiaoDiemBIH *mangGiaoDiem, unsigned int chiSoGiaoDiem,
                          ThongTinOChuNhat *thongTinOChuNhat );

// ---- tính ô
void lapVongO( PhimTruong *PhimTruong, Anh *anh, unsigned short beRongO, unsigned short  beCaoO, unsigned char tinhTiep );
void tinhO( ThongTinOChuNhat *thongTinOChuNhat, PhimTruong *phimTruong, GiaoDiemBIH *mangGiaoDiem, unsigned int chiSoGiaoDiem, Tia *tia, Vecto *goc, Vecto *huongDoc, Vecto *huongNgang, Vecto *buocNgang, Vecto *buocDoc );

// ---- tính ô - cho khởi động tính tiếp
unsigned char timTepVaThongTinO_TinhTiep( char *tenTep, ThongTinOChuNhat *thongTinOChuNhat, Anh *anh );

// ---- dó tia
Mau doTia( Tia *tia, PhimTruong *phimTruong, GiaoDiemBIH *mangGiaoDiem, unsigned int chiSoGiaoDiem, unsigned short soNhoi );

int main( int argc, char **argv ) {
   
   PhimTruong phimTruong;
   unsigned int soPhimTruong = 0;
   docThamSoPhimTruong( argc, argv, &soPhimTruong );
   
   if( soPhimTruong == 0 )
      phimTruong = datPhimTruongSo0( argc, argv );
   else if( soPhimTruong == 1 )
      phimTruong = datPhimTruongSo1( argc, argv );
   else if( soPhimTruong == 2 )
      phimTruong = datPhimTruongSo2( argc, argv );
   else if( soPhimTruong == 3 )
      phimTruong = datPhimTruongSo3( argc, argv );
   // ---- phim trường thử nghiệm
   else
      phimTruong = datPhimTruongSoTN( argc, argv );
   
   // ---- giữ số phim trường
   phimTruong.so = soPhimTruong;
   
   // -----
   unsigned int soHoatHinhDau;
   unsigned int soHoatHinhCuoi;
   docThamSoHoatHinh( argc, argv, &soHoatHinhDau, &soHoatHinhCuoi );
   phimTruong.soHoatHinhDau = soHoatHinhDau;
   phimTruong.soHoatHinhCuoi = soHoatHinhCuoi;

   // ---- cỡ kích ảnh
   unsigned int beCaoAnh;// = 201;//601;  // 601
   unsigned int beRongAnh;// = beCaoAnh << 1;
   // ---- cho hình sẽ phóng to theo cỡ kích ảnh
   float coKichDiemAnh;// = 0.005f*3.0f;   // 1.0f;
   docThamCoKich( argc, argv, &beRongAnh, &beCaoAnh, &coKichDiemAnh );
   
   // ---- xem xài ô
   unsigned char xaiO;
   unsigned short beRongO;
   unsigned short beCaoO;
   unsigned char tinhTiep;
   docThamXaiO( argc, argv, &xaiO, &beRongO, &beCaoO, &tinhTiep );
   
   printf( "Cỡ Kích Ảnh: %d %d %5.4f  xài ô: %d\n", beRongAnh, beCaoAnh, coKichDiemAnh, xaiO );
   
   Anh anh = taoAnhVoiCoKich( beRongAnh, beCaoAnh, coKichDiemAnh );
   
   if( xaiO )
      lapVongO( &phimTruong, &anh, beRongO, beCaoO, tinhTiep );
   else
      lapVongHang( &phimTruong, &anh );

   time_t thoiGianBatDauToanCau;
   time(&thoiGianBatDauToanCau);

   printf( "Cỡ kích các ảnh: %d %d\n", beRongAnh, beCaoAnh );

   // ---- xóa danh sách vật thể
   huyDanhSachVatThe( phimTruong.danhSachVatThe, phimTruong.soLuongVatThe );
   
   // ---- xong công việc, xóa ảnh
   xoaAnh( &anh );

   return 1;
}


#pragma mark ---- Vẽ Ảnh Hàng
void lapVongHang( PhimTruong *phimTruong, Anh *anh ) {

   unsigned char soPhimTruong = phimTruong->so;
   
   printf( "PhimTrường %d  số lượng vật thể %d/%d\n", soPhimTruong, phimTruong->soLuongVatThe, kSO_LUONG_VAT_THE_TOI_DA );
   printf( "     bứcẢnhĐầu %d   bứcẢnhCuối %d\n", phimTruong->soHoatHinhDau, phimTruong->soHoatHinhCuoi );

   while( phimTruong->soHoatHinhDau < phimTruong->soHoatHinhCuoi ) {

      // ---- nâng cấp hoạt hình
      if( soPhimTruong == 0 )
         nangCapPhimTruong0( phimTruong );
      else if( soPhimTruong == 1 )
         nangCapPhimTruong1( phimTruong );
      else if( soPhimTruong == 2 )
         nangCapPhimTruong2( phimTruong );
      else if( soPhimTruong == 3 )
         nangCapPhimTruong3( phimTruong );
      // ---- phim trường thử nghiệm
      else
         nangCapPhimTruongTN( phimTruong );

      //      xemVatThe( phimTruong.danhSachVatThe, phimTruong.soLuongVatThe );
      char tenAnh[256];
      sprintf( tenAnh, "TGB_%02d_%04d.exr", phimTruong->so, phimTruong->soHoatHinhDau );
      datTenAnh( anh, tenAnh );
      
      // ---- xây tầng bậc đoạn bao bì
      phimTruong->baoBi = tinhBaoBiTGChoDanhSachVatThe( phimTruong->danhSachVatThe, phimTruong->soLuongVatThe );

      unsigned int soLuongGiaoDiem = 0;
      
      datLaiMangChiVatThe( phimTruong->mangChiSoVatTheSapXep, phimTruong->soLuongVatThe );
      // ==== tạo tầng bậc bao bì
      // ---- trục và tọa độ mặt phẳng đầu
      float toaDoMatPhang;
      unsigned int truc;
      tinhTrucVaToaMatPhangChia( &(phimTruong->baoBi), &truc, &toaDoMatPhang );

      // ---- tạo cây tầng bậc bao bì
      GiaoDiemBIH mangGiaoDiem[kSO_LUONG__GIAO_DIEM_TOI_DA];  // 28672/7 ≈ 16384 vật thể
      
      chiaVatThe( phimTruong->danhSachVatThe, phimTruong->mangChiSoVatTheSapXep, phimTruong->soLuongVatThe, 0, &(phimTruong->baoBi), truc, toaDoMatPhang, mangGiaoDiem, &soLuongGiaoDiem );

      //      xemCay( mangGiaoDiem, chiSoGiaoDiem );
      printf( "cây chiSoGiaoDiem %d\n", soLuongGiaoDiem );
      // ==== thời gian bặt đầu tính
      time_t thoiGianBatDau;
      time(&thoiGianBatDau);
      
      // ==== vẽ ảnh (kết xuất)
      if( phimTruong->mayQuayPhim.kieuChieu == kKIEU_CHIEU__PHOI_CANH )
         veAnhChieuPhoiCanh( anh, phimTruong, mangGiaoDiem, soLuongGiaoDiem );
      else if( phimTruong->mayQuayPhim.kieuChieu == kKIEU_CHIEU__TRUC_GIAO )
         veAnhChieuTrucGiao( anh, phimTruong, mangGiaoDiem, soLuongGiaoDiem );
      else if( phimTruong->mayQuayPhim.kieuChieu == kKIEU_CHIEU__TOAN_CANH )
         veAnhChieuToanCanh( anh, phimTruong, mangGiaoDiem, soLuongGiaoDiem );
      
      // ==== thời gian xong
      time_t thoiGianHienTai;
      time(&thoiGianHienTai);
      time_t thoiGianKetXuat = thoiGianHienTai - thoiGianBatDau;
      struct tm *hienTai = localtime( &thoiGianHienTai );
      
      // ---- báo số ảnh đã lưu xong và thời gian
      printf( "-->> %s <<--  %ld giây (%5.2f phút) %d/%d    %d.%02d.%02d  %02d:%02d:%02d\n", tenAnh, thoiGianKetXuat, (float)thoiGianKetXuat/60.0f,
             phimTruong->soHoatHinhDau, phimTruong->soHoatHinhCuoi, 1900 + hienTai->tm_year, hienTai->tm_mon+1, hienTai->tm_mday, hienTai->tm_hour, hienTai->tm_min, hienTai->tm_sec );
      
      // ---- lưu ảnh
      luuAnhEXR_ZIP( anh, kKIEU_HALF, (unsigned short)thoiGianKetXuat );
      

   }
}

#pragma mark ---- CHIẾU PHỐI CẢNH
void veAnhChieuPhoiCanh( Anh *anh, PhimTruong *phimTruong, GiaoDiemBIH *mangGiaoDiem, unsigned int chiSoGiaoDiem ) {
   
   MayQuayPhim *mayQuayPhim = &(phimTruong->mayQuayPhim);
   // ---- góc tia là vị trí máy quay phim
   Tia tia;
   tia.goc.x = mayQuayPhim->viTri.x;
   tia.goc.y = mayQuayPhim->viTri.y;
   tia.goc.z = mayQuayPhim->viTri.z;
   
   //   printf( "Tia %5.3f %5.3f %5.3f\n", tia.goc.x, tia.goc.y, tia.goc.z );
   unsigned short beRong = anh->beRong;
   unsigned short beCao = anh->beCao;
   
   //   printf( "Tia %5.3f %5.3f %5.3f\n", gocX, gocY, gocZ );
   // ---- tính vectơ hướng lên (dộc)
   Vecto huongDoc;  // hướng dọc
   huongDoc.x = mayQuayPhim->xoay[4];
   huongDoc.y = mayQuayPhim->xoay[5];
   huongDoc.z = mayQuayPhim->xoay[6];
   donViHoa( &huongDoc );
   
   Vecto huongNgang;  // hướng ngang
   huongNgang.x = -mayQuayPhim->xoay[0];
   huongNgang.y = -mayQuayPhim->xoay[1];
   huongNgang.z = -mayQuayPhim->xoay[2];
   donViHoa( &huongNgang );
//   printf( "doTia: huongNgang %5.3f %5.3f %5.3f\n", huongNgang.x, huongNgang.y, huongNgang.z );
//   printf( "doTia: huongDoc %5.3f %5.3f %5.3f\n\n", huongDoc.x, huongDoc.y, huongDoc.z );
   
   // ---- tính vectơ cho bước ngang
   Vecto buocNgang;
   buocNgang.x = huongNgang.x*anh->coKichDiemAnh;
   buocNgang.y = huongNgang.y*anh->coKichDiemAnh;
   buocNgang.z = huongNgang.z*anh->coKichDiemAnh;
   
   // ---- tính vectơ cho bước lên (dọc)
   Vecto buocDoc;  // bước dọc
   buocDoc.x = huongDoc.x*anh->coKichDiemAnh;
   buocDoc.y = huongDoc.y*anh->coKichDiemAnh;
   buocDoc.z = huongDoc.z*anh->coKichDiemAnh;
   //   printf( "buocgNgang %5.3f %5.3f %5.3f\n", buocNgang.x, buocNgang.y, buocNgang.z );
   //   printf( "buocDoc %5.3f %5.3f %5.3f\n\n", buocDoc.x, buocDoc.y, buocDoc.z );
   
   // ---- tính điểm góc ảnh (trái dưới)
   float gocX = -(beCao >> 1)*buocDoc.x -(beRong >> 1)*buocNgang.x + tia.goc.x + mayQuayPhim->cachManChieu*mayQuayPhim->xoay[8];
   float gocY = -(beCao >> 1)*buocDoc.y -(beRong >> 1)*buocNgang.y + tia.goc.y + mayQuayPhim->cachManChieu*mayQuayPhim->xoay[9];
   float gocZ = -(beCao >> 1)*buocDoc.z -(beRong >> 1)*buocNgang.z + tia.goc.z + mayQuayPhim->cachManChieu*mayQuayPhim->xoay[10];
   //   printf( "goc anh %5.3f %5.3f %5.3f\n", gocX, gocY, gocZ );
   //   printf( "tia.goc %5.3f %5.3f %5.3f\n", tia.goc.x, tia.goc.y, tia.goc.z );
   //   exit(0);
   // ---- xâu phim trường
   char xauSoPhimTrung[3];
   
   if( phimTruong->so < 4 ) {
      xauSoPhimTrung[0] = '0';
      xauSoPhimTrung[1] = '0' + phimTruong->so;
   }
   else {
      xauSoPhimTrung[0] = 'T';
      xauSoPhimTrung[1] = 'N';
   }
   xauSoPhimTrung[2] = 0x00;
   
   unsigned long int chiSoAnh = 0;
   
   short hang = beCao - 1;
   while( hang > -1 ) {
      
      // ---- cho biết khi đến hàng mới
      printf( "hàng %4d/%d (%d‰)   ảnh %d/%d  PT %s - Phối Cảnh\n", hang, beCao, hang*1000/beCao, phimTruong->soHoatHinhDau, phimTruong->soHoatHinhCuoi, xauSoPhimTrung );
      
      // ---- tính hướng cho tia của điểm ảnh này
      tia.huong.x = gocX + buocDoc.x*hang - tia.goc.x;
      tia.huong.y = gocY + buocDoc.y*hang - tia.goc.y;
      tia.huong.z = gocZ + buocDoc.z*hang - tia.goc.z;
      
      unsigned short cot = 0;
      while( cot < beRong ) {
 //        printf( "\ncot %d ", cot );
         Mau mauDoTia = doTia( &tia, phimTruong, mangGiaoDiem, chiSoGiaoDiem, 0 );
         Mau mauBauTroi = hoaTietBauTroi( tia.huong, &(phimTruong->hoaTietBauTroi) );
         Mau mauCoSuongMu = toSuongMu( &(tia.huong), &mauDoTia, &mauBauTroi );
         
         // ---- giữ điểm ảnh kết xuất
         anh->kenhDo[chiSoAnh] = mauCoSuongMu.d;
         anh->kenhLuc[chiSoAnh] = mauCoSuongMu.l;
         anh->kenhXanh[chiSoAnh] = mauCoSuongMu.x;
         anh->kenhDuc[chiSoAnh] = mauDoTia.dd;
         anh->kenhXa[chiSoAnh] = mauDoTia.c;
         
         chiSoAnh++;
         
         // ---- hướng tia tiếp
         tia.huong.x += buocNgang.x;
         tia.huong.y += buocNgang.y;
         tia.huong.z += buocNgang.z;
   
 //        if( !(hang % 16) && !(cot % 16) )
 //           printf( "%5.3e\n", mauDoTia.c );
         
         cot++;
      }
 //     printf( "\n" );
      hang--;
   }
   
}

#pragma mark ---- CHIẾU TRỰC GIAO
void veAnhChieuTrucGiao( Anh *anh, PhimTruong *phimTruong, GiaoDiemBIH *mangGiaoDiem, unsigned int chiSoGiaoDiem ) {
   
   MayQuayPhim *mayQuayPhim = &(phimTruong->mayQuayPhim);
   
   //   printf( "Tia %5.3f %5.3f %5.3f\n", tia.goc.x, tia.goc.y, tia.goc.z );
   unsigned short beRong = anh->beRong;
   unsigned short beCao = anh->beCao;
   
   //   printf( "Tia %5.3f %5.3f %5.3f\n", gocX, gocY, gocZ );
   // ---- tính vectơ hướng lên (dộc)
   Vecto huongDoc;  // hướng dọc
   huongDoc.x = mayQuayPhim->xoay[4];
   huongDoc.y = mayQuayPhim->xoay[5];
   huongDoc.z = mayQuayPhim->xoay[6];
   donViHoa( &huongDoc );
   
   Vecto huongNgang;  // hướng ngang
   huongNgang.x = -mayQuayPhim->xoay[0];
   huongNgang.y = -mayQuayPhim->xoay[1];
   huongNgang.z = -mayQuayPhim->xoay[2];
   donViHoa( &huongNgang );
   //   printf( "huongNgang %5.3f %5.3f %5.3f\n", huongNgang.x, huongNgang.y, huongNgang.z );
   //   printf( "huongDoc %5.3f %5.3f %5.3f\n\n", huongDoc.x, huongDoc.y, huongDoc.z );
   
   // ---- tính vectơ cho bước ngang
   Vecto buocNgang;
   buocNgang.x = huongNgang.x*anh->coKichDiemAnh;
   buocNgang.y = huongNgang.y*anh->coKichDiemAnh;
   buocNgang.z = huongNgang.z*anh->coKichDiemAnh;
   
   // ---- tính vectơ cho bước lên (dọc)
   Vecto buocDoc;  // bước dọc
   buocDoc.x = -huongDoc.x*anh->coKichDiemAnh;
   buocDoc.y = -huongDoc.y*anh->coKichDiemAnh;
   buocDoc.z = -huongDoc.z*anh->coKichDiemAnh;
   //      printf( "buocgNgang %5.3f %5.3f %5.3f\n", buocNgang.x, buocNgang.y, buocNgang.z );
   //      printf( "buocDoc %5.3f %5.3f %5.3f\n\n", buocDoc.x, buocDoc.y, buocDoc.z );
   
   // ---- tính điểm góc ảnh (trái dưới)
   float gocX = -(beCao >> 1)*buocDoc.x -(beRong >> 1)*buocNgang.x + mayQuayPhim->viTri.x;
   float gocY = -(beCao >> 1)*buocDoc.y -(beRong >> 1)*buocNgang.y + mayQuayPhim->viTri.y;
   float gocZ = -(beCao >> 1)*buocDoc.z -(beRong >> 1)*buocNgang.z + mayQuayPhim->viTri.z;
   //      printf( "goc anh %5.3f %5.3f %5.3f\n", gocX, gocY, gocZ );
   // ---- tính hướng cho tia - lần nào songๆ nhau
   Tia tia;
   tia.huong.x = mayQuayPhim->xoay[8];
   tia.huong.y = mayQuayPhim->xoay[9];
   tia.huong.z = mayQuayPhim->xoay[10];
   //   printf( "tia.huong %5.3f %5.3f %5.3f\n", tia.huong.x, tia.huong.y, tia.huong.z );
   //  exit(0);
   // ---- xâu phim trường
   char xauSoPhimTrung[3];
   
   if( phimTruong->so < 4 ) {
      xauSoPhimTrung[0] = '0';
      xauSoPhimTrung[1] = '0' + phimTruong->so;
   }
   else {
      xauSoPhimTrung[0] = 'T';
      xauSoPhimTrung[1] = 'N';
   }
   xauSoPhimTrung[2] = 0x00;
   
   unsigned int chiSoAnh = 0;
   
   unsigned short hang = 0;
   while( hang < beCao ) {
      
      // ---- cho biết khi đến hàng mới
      printf( "hàng %4d/%d (%d‰)   ảnh %d/%d  PT %s - Trực Giao\n", hang, beCao, hang*1000/beCao, phimTruong->soHoatHinhDau, phimTruong->soHoatHinhCuoi, xauSoPhimTrung );
      
      // ---- tính hướng cho tia của điểm ảnh này
      tia.goc.x = gocX + buocDoc.x*hang;
      tia.goc.y = gocY + buocDoc.y*hang;
      tia.goc.z = gocZ + buocDoc.z*hang;
      
      unsigned short cot = 0;
      while( cot < beRong ) {
         Mau mauDoTia = doTia( &tia, phimTruong, mangGiaoDiem, chiSoGiaoDiem, 0 );
         Mau mauBauTroi = hoaTietBauTroi( tia.huong, &(phimTruong->hoaTietBauTroi) );
         Mau mauCoSuongMu = toSuongMu( &(tia.huong), &mauDoTia, &mauBauTroi );
         
         // ---- giữ điểm ảnh kết xuất
         anh->kenhDo[chiSoAnh] = mauCoSuongMu.d;
         anh->kenhLuc[chiSoAnh] = mauCoSuongMu.l;
         anh->kenhXanh[chiSoAnh] = mauCoSuongMu.x;
         anh->kenhDuc[chiSoAnh] = mauDoTia.dd;
         anh->kenhXa[chiSoAnh] = mauDoTia.c;
         chiSoAnh++;
         
         // ---- hướng tia tiếp
         tia.goc.x += buocNgang.x;
         tia.goc.y += buocNgang.y;
         tia.goc.z += buocNgang.z;
         
         cot++;
      }
      
      hang++;
   }
   
}



#pragma mark ---- CHIẾU TOÀN CẢNH


//   sin(gocNgang)*huongNgang + cos(gocNgang)*huongNhin*cos(gocDoc) + sin(gocDoc)*huongDoc
//   donViHoa


void veAnhChieuToanCanh( Anh *anh, PhimTruong *phimTruong, GiaoDiemBIH *mangGiaoDiem, unsigned int chiSoGiaoDiem ) {
   
   MayQuayPhim *mayQuayPhim = &(phimTruong->mayQuayPhim);
   // ---- góc tia là vị trí máy quay phim
   Tia tia;
   tia.goc.x = mayQuayPhim->viTri.x;
   tia.goc.y = mayQuayPhim->viTri.y;
   tia.goc.z = mayQuayPhim->viTri.z;
   
//   printf( "ToanCanh: Tia %5.3f %5.3f %5.3f\n", tia.goc.x, tia.goc.y, tia.goc.z );
   unsigned short beRong = anh->beRong;
   unsigned short beCao = anh->beCao;

   // ---- tính vectơ hướng lên (dộc)
   Vecto huongDoc;  // hướng dọc
   huongDoc.x = mayQuayPhim->xoay[4];
   huongDoc.y = mayQuayPhim->xoay[5];
   huongDoc.z = mayQuayPhim->xoay[6];
   donViHoa( &huongDoc );
   
   Vecto huongNgang;  // hướng ngang
   huongNgang.x = -mayQuayPhim->xoay[0];
   huongNgang.y = -mayQuayPhim->xoay[1];
   huongNgang.z = -mayQuayPhim->xoay[2];
   donViHoa( &huongNgang );
   
//   printf( "huongNgang %5.3f %5.3f %5.3f\n", huongNgang.x, huongNgang.y, huongNgang.z );
//   printf( "huongDoc %5.3f %5.3f %5.3f\n\n", huongDoc.x, huongDoc.y, huongDoc.z );

   // ---- vecto nhìn (trái dưới)
   Vecto huongMayQuayPhimNhin;
   huongMayQuayPhimNhin.x = mayQuayPhim->xoay[8];
   huongMayQuayPhimNhin.y = mayQuayPhim->xoay[9];
   huongMayQuayPhimNhin.z = mayQuayPhim->xoay[10];
   
   // ---- tính góc đến góc ảnh (rad)
   float gocNgang = -(beRong >> 1)*anh->coKichDiemAnh;  // chia 2 cho nữa ảnh
   float gocDoc = (beCao >> 1)*anh->coKichDiemAnh;  // chia 2 cho nữa ảnh

   //   printf( "goc anh %5.3f %5.3f %5.3f\n", gocX, gocY, gocZ );
   //   printf( "tia.goc %5.3f %5.3f %5.3f\n", tia.goc.x, tia.goc.y, tia.goc.z );
   //   exit(0);
   // ---- xâu phim trường
   char xauSoPhimTrung[3];
   
   if( phimTruong->so < 4 ) {
      xauSoPhimTrung[0] = '0';
      xauSoPhimTrung[1] = '0' + phimTruong->so;
   }
   else {
      xauSoPhimTrung[0] = 'T';
      xauSoPhimTrung[1] = 'N';
   }
   xauSoPhimTrung[2] = 0x00;
   
   unsigned int chiSoAnh = 0;
   
   // ---- đến hàng đầu
   float gocXoayDoc = gocDoc;
   float cosGocXoayDoc = cosf( gocDoc );
   float sinGocXoayDoc = sinf( gocDoc );
   
   // ---- trục xoay dọc là hướng ngang của máy quay phim
//   Vecto trucXoayDoc = huongNgang;

//   printf( "huongNhin %5.3f %5.3f %5.3f  huongNgang %5.3f %5.3f %5.3f  huongDoc %5.3f %5.3f %5.3f\n",
//          huongMayQuayPhimNhin.x, huongMayQuayPhimNhin.y, huongMayQuayPhimNhin.z,
//          huongNgang.x, huongNgang.y, huongNgang.z,
//          huongDoc.x, huongDoc.y, huongDoc.z);
   
   unsigned short hang = 0;
   while( hang < beCao ) {
      
      // ---- cho biết khi đến hàng mới
      printf( "hàng %4d/%d (%d‰)   ảnh %d/%d  PT %s - Toàn Cảnh)\n", hang, beCao, hang*1000/beCao, phimTruong->soHoatHinhDau, phimTruong->soHoatHinhCuoi, xauSoPhimTrung );


      // ---- xoay hướng nhìn đến goc xoay dọc
 //     Vecto huongNhinTuongDoi = xoayVectoQuanhTruc( &huongMayQuayPhimNhin, &trucXoayDoc, gocXoayDoc );
      
   //   printf( "*** huongNhinTuongDoi %5.3f %5.3f %5.3f\n", huongNhinTuongDoi.x, huongNhinTuongDoi.y, huongNhinTuongDoi.z );
      
      // ---- tính trục xoay ngang cho hàng này
 //     Vecto trucXoayNgang = tichCoHuong( &trucXoayDoc, &huongNhinTuongDoi );
      
      // ---- chỉnh bởi goc từ mặt phẳng nhìn ngang của máy quay phim 
      float buocXoayNgan = anh->coKichDiemAnh;
      
      // ---- đến cột đầu
      // +-+--------+
      // | |        |
      // | |        |
      // +-+--------+
      float gocXoayNgang = gocNgang;
      float cosGocXoayNgang = cosf( gocXoayNgang );
      float sinGocXoayNgang = sinf( gocXoayNgang );

      //      exit(0);
      unsigned short cot = 0;
      while( cot < beRong ) {
   //      printf( "======= cot %d  hang %d  | gocXoayNgang %5.3f cos %5.3f sin %5.3f | gocXoayDoc %5.3f cos %5.3f sin %5.3f\n", cot, hang,
   //             gocXoayNgang, cosGocXoayNgang, sinGocXoayNgang,
   //             gocXoayDoc, cosGocXoayDoc, sinGocXoayDoc );
         
         tia.huong.x = huongNgang.x*sinGocXoayNgang + huongMayQuayPhimNhin.x*cosGocXoayNgang*cosGocXoayDoc + huongDoc.x*sinGocXoayDoc;
         tia.huong.y = huongNgang.y*sinGocXoayNgang + huongMayQuayPhimNhin.y*cosGocXoayNgang*cosGocXoayDoc + huongDoc.y*sinGocXoayDoc;
         tia.huong.z = huongNgang.z*sinGocXoayNgang + huongMayQuayPhimNhin.z*cosGocXoayNgang*cosGocXoayDoc + huongDoc.z*sinGocXoayDoc;
         donViHoa( &(tia.huong) );
         
    //     printf( "    tia.huong %5.3f; %5.3f; %5.3f - doDai %5.3f\n", tia.huong.x, tia.huong.y, tia.huong.z,
      //          tia.huong.x*tia.huong.x + tia.huong.y*tia.huong.y + tia.huong.z*tia.huong.z );
         
         Mau mauDoTia = doTia( &tia, phimTruong, mangGiaoDiem, chiSoGiaoDiem, 0 );
         Mau mauBauTroi = hoaTietBauTroi( tia.huong, &(phimTruong->hoaTietBauTroi) );
         Mau mauCoSuongMu = toSuongMu( &(tia.huong), &mauDoTia, &mauBauTroi );
         
         // ---- giữ điểm ảnh kết xuất
         anh->kenhDo[chiSoAnh] = mauCoSuongMu.d;
         anh->kenhLuc[chiSoAnh] = mauCoSuongMu.l;
         anh->kenhXanh[chiSoAnh] = mauCoSuongMu.x;
         anh->kenhDuc[chiSoAnh] = mauDoTia.dd;
         anh->kenhXa[chiSoAnh] = mauDoTia.c;
         
         chiSoAnh++;
         
         // ---- hướng tia tiếp
         gocXoayNgang += buocXoayNgan;
         cosGocXoayNgang = cosf( gocXoayNgang );
         sinGocXoayNgang = sinf( gocXoayNgang );
         
         cot++;
      }
      // ---- nâng cấp
      gocXoayDoc -= anh->coKichDiemAnh;
      cosGocXoayDoc = cosf( gocXoayDoc );
      sinGocXoayDoc = sinf( gocXoayDoc );
      
      hang++;
   }
   
}

/*void veAnhChatLuongCao( Anh *anh, PhimTruong *phimTruong, GiaoDiemBIH *mangGiaoDiem, unsigned int chiSoGiaoDiem ) {
 
 MayQuayPhim *mayQuayPhim = &(phimTruong->mayQuayPhim);
 Tia tia;
 tia.goc.x = mayQuayPhim->viTri.x;
 tia.goc.y = mayQuayPhim->viTri.y;
 tia.goc.z = mayQuayPhim->viTri.z;
 
 //   printf( "Tia %5.3f %5.3f %5.3f\n", tia.goc.x, tia.goc.y, tia.goc.z );
 unsigned short beRong = anh->beRong;
 unsigned short beCao = anh->beCao;
 
 //   printf( "Tia %5.3f %5.3f %5.3f\n", gocX, gocY, gocZ );
 // ---- tính vectơ hướng lên (dộc)
 Vecto huongDoc;  // hướng dọc
 huongDoc.x = mayQuayPhim->xoay[4];
 huongDoc.y = mayQuayPhim->xoay[5];
 huongDoc.z = mayQuayPhim->xoay[6];
 donViHoa( &huongDoc );
 
 Vecto huongNgang;  // hướng ngang
 huongNgang.x = -mayQuayPhim->xoay[0];
 huongNgang.y = -mayQuayPhim->xoay[1];
 huongNgang.z = -mayQuayPhim->xoay[2];
 donViHoa( &huongNgang );
 //   printf( "huongNgang %5.3f %5.3f %5.3f\n", huongNgang.x, huongNgang.y, huongNgang.z );
 //   printf( "huongDoc %5.3f %5.3f %5.3f\n\n", huongDoc.x, huongDoc.y, huongDoc.z );
 
 // ---- tính vectơ cho bước ngang
 Vecto buocNgang;
 buocNgang.x = huongNgang.x*anh->coKichDiemAnh;
 buocNgang.y = huongNgang.y*anh->coKichDiemAnh;
 buocNgang.z = huongNgang.z*anh->coKichDiemAnh;
 Vecto buocNgang_25;
 buocNgang_25.x = buocNgang.x*0.25f;
 buocNgang_25.y = buocNgang.y*0.25f;
 buocNgang_25.z = buocNgang.z*0.25f;
 
 // ---- tính vectơ cho bước lên (dộc)
 Vecto buocDoc;  // bước dọc
 buocDoc.x = huongDoc.x*anh->coKichDiemAnh;
 buocDoc.y = huongDoc.y*anh->coKichDiemAnh;
 buocDoc.z = huongDoc.z*anh->coKichDiemAnh;
 Vecto buocDoc_25;
 buocDoc_25.x = buocDoc.x*0.25f;
 buocDoc_25.y = buocDoc.y*0.25f;
 buocDoc_25.z = buocDoc.z*0.25f;
 //   printf( "buocgNgang %5.3f %5.3f %5.3f\n", buocNgang.x, buocNgang.y, buocNgang.z );
 //   printf( "buocDoc %5.3f %5.3f %5.3f\n\n", buocDoc.x, buocDoc.y, buocDoc.z );
 
 // ---- tính điểm góc ảnh (trái dưới)
 float gocX = -(beCao >> 1)*buocDoc.x -(beRong >> 1)*buocNgang.x + tia.goc.x + mayQuayPhim->cachManChieu*mayQuayPhim->xoay[8];
 float gocY = -(beCao >> 1)*buocDoc.y -(beRong >> 1)*buocNgang.y + tia.goc.y + mayQuayPhim->cachManChieu*mayQuayPhim->xoay[9];
 float gocZ = -(beCao >> 1)*buocDoc.z -(beRong >> 1)*buocNgang.z + tia.goc.z + mayQuayPhim->cachManChieu*mayQuayPhim->xoay[10];
 //   printf( "goc anh %5.3f %5.3f %5.3f\n", gocX, gocY, gocZ );
 //   printf( "tia.goc %5.3f %5.3f %5.3f\n", tia.goc.x, tia.goc.y, tia.goc.z );
 //   exit(0);
 unsigned int chiSoAnh = 0;
 
 unsigned short hang = 0;
 while( hang < beCao ) {
 
 // ---- tính hướng cho tia của điểm ảnh này
 tia.huong.x = gocX + buocDoc.x*hang - tia.goc.x;
 tia.huong.y = gocY + buocDoc.y*hang - tia.goc.y;
 tia.huong.z = gocZ + buocDoc.z*hang - tia.goc.z;
 
 unsigned short cot = 0;
 while( cot < beRong ) {
 
 Tia tiaPhu;
 tiaPhu.goc = tia.goc;
 // -----
 tiaPhu.huong.x = tia.huong.x - 0.25f*buocNgang.x - 0.25f*buocDoc.x;
 tiaPhu.huong.y = tia.huong.y - 0.25f*buocNgang.y - 0.25f*buocDoc.y;
 tiaPhu.huong.z = tia.huong.z - 0.25f*buocNgang.z - 0.25f*buocDoc.z;
 Mau mauDoTia = doTia( &tiaPhu, phimTruong, mangGiaoDiem, 0 );
 Mau mauBauTroi = hoaTietBauTroi( tiaPhu.huong, &(phimTruong->hoaTietBauTroi) );
 Mau mauCoSuongMu0 = toSuongMu( &(tiaPhu.huong), &mauDoTia, &mauBauTroi );
 
 // ----
 tiaPhu.huong.x = tia.huong.x + 0.25f*buocNgang.x - 0.25f*buocDoc.x;
 tiaPhu.huong.y = tia.huong.y + 0.25f*buocNgang.y - 0.25f*buocDoc.y;
 tiaPhu.huong.z = tia.huong.z + 0.25f*buocNgang.z - 0.25f*buocDoc.z;
 mauDoTia = doTia( &tiaPhu, phimTruong, mangGiaoDiem, 0 );
 mauBauTroi = hoaTietBauTroi( tiaPhu.huong, &(phimTruong->hoaTietBauTroi) );
 Mau mauCoSuongMu1 = toSuongMu( &(tiaPhu.huong), &mauDoTia, &mauBauTroi );
 
 // ----
 tiaPhu.huong.x = tia.huong.x + 0.25f*buocNgang.x - 0.25f*buocDoc.x;
 tiaPhu.huong.y = tia.huong.y + 0.25f*buocNgang.y - 0.25f*buocDoc.y;
 tiaPhu.huong.z = tia.huong.z + 0.25f*buocNgang.z - 0.25f*buocDoc.z;
 mauDoTia = doTia( &tiaPhu, phimTruong, mangGiaoDiem, 0 );
 mauBauTroi = hoaTietBauTroi( tiaPhu.huong, &(phimTruong->hoaTietBauTroi) );
 Mau mauCoSuongMu2 = toSuongMu( &(tiaPhu.huong), &mauDoTia, &mauBauTroi );
 
 // ----
 tiaPhu.huong.x = tia.huong.x + 0.25f*buocNgang.x + 0.25f*buocDoc.x;
 tiaPhu.huong.y = tia.huong.y + 0.25f*buocNgang.y + 0.25f*buocDoc.y;
 tiaPhu.huong.z = tia.huong.z + 0.25f*buocNgang.z + 0.25f*buocDoc.z;
 mauDoTia = doTia( &tiaPhu, phimTruong, mangGiaoDiem, 0 );
 mauBauTroi = hoaTietBauTroi( tiaPhu.huong, &(phimTruong->hoaTietBauTroi) );
 Mau mauCoSuongMu3 = toSuongMu( &(tiaPhu.huong), &mauDoTia, &mauBauTroi );
 
 // ---- giữ điểm ảnh kết xuất
 anh->kenhDo[chiSoAnh] = 0.25f*(mauCoSuongMu0.d + mauCoSuongMu1.d + mauCoSuongMu2.d + mauCoSuongMu3.d);
 anh->kenhLuc[chiSoAnh] = 0.25f*(mauCoSuongMu0.l + mauCoSuongMu1.l + mauCoSuongMu2.l + mauCoSuongMu3.l);
 anh->kenhXanh[chiSoAnh] = 0.25f*(mauCoSuongMu0.x + mauCoSuongMu1.x + mauCoSuongMu2.x + mauCoSuongMu3.x);
 anh->kenhDuc[chiSoAnh] = 1.0f;//mauDoTia.dd;
 anh->kenhXa[chiSoAnh] = mauDoTia.c;
 
 chiSoAnh++;
 
 // ---- hướng tia tiếp
 tia.huong.x += buocNgang.x;
 tia.huong.y += buocNgang.y;
 tia.huong.z += buocNgang.z;
 
 cot++;
 }
 
 hang++;
 }
 }*/

#pragma mark ---- Vẽ Ảnh Ô
void lapVongO( PhimTruong *phimTruong, Anh *anh, unsigned short beRongO, unsigned short  beCaoO, unsigned char tinhTiep ) {
   
   unsigned short soPhimTruong = phimTruong->so;
   printf( "PhimTrường %d  số lượng vật thể %d/%d\n", soPhimTruong, phimTruong->soLuongVatThe, kSO_LUONG_VAT_THE_TOI_DA );
   printf( "     bứcẢnhĐầu %d   bứcẢnhCuối %d\n", phimTruong->soHoatHinhDau, phimTruong->soHoatHinhCuoi );
   printf( "     ô: bề rộng %d   bề cao %d\n     tính tiếp %d\n", beRongO, beCaoO, tinhTiep );

   ThongTinOChuNhat thongTinOChuNhat;
   datThongTinOChuNhat( &thongTinOChuNhat, beRongO, beCaoO, anh->beRong, anh->beCao );

   while( phimTruong->soHoatHinhDau < phimTruong->soHoatHinhCuoi ) {
      printf( "0 phimTruong->soHoatHinhDau %d   phimTruong->soHoatHinhCuoi %d \n", phimTruong->soHoatHinhDau, phimTruong->soHoatHinhCuoi );
      
      // ---- nâng cấp hoạt hình
      if( soPhimTruong == 0 )
         nangCapPhimTruong0( phimTruong );
      else if( soPhimTruong == 1 )
         nangCapPhimTruong1( phimTruong );
      else if( soPhimTruong == 2 )
         nangCapPhimTruong2( phimTruong );
      else if( soPhimTruong == 3 )
         nangCapPhimTruong3( phimTruong );
      
      //      xemVatThe( phimTruong->danhSachVatThe, phimTruong->soLuongVatThe );
      char tenAnh[256];
      sprintf( tenAnh, "TGB_%02d_%04d.exr", phimTruong->so, phimTruong->soHoatHinhDau );
      datTenAnh( anh, tenAnh );
      
      // ---- sau có tên ảnh, xem có cần tính tiếp
      if( tinhTiep ) {
         unsigned char coTep = timTepVaThongTinO_TinhTiep( anh->ten, &thongTinOChuNhat, anh);
         if( coTep ) {
            thongTinOChuNhat.tinhTiep = tinhTiep;
         }
         else {
            printf( "Vấn đề mở tiếp, bắt đầ tính từ đầu.\n");
            thongTinOChuNhat.tinhTiep = kSAI;
         }
         // ---- sau khởi động lại không cần tính tiếp nữa
         tinhTiep = kSAI;
      }
      else
         chuanBiLuuAnhEXR_O_ZIP( anh, &thongTinOChuNhat, kKIEU_HALF, 0 );
      
      // ---- xây tầng bậc đoạn bao bì
      phimTruong->baoBi = tinhBaoBiTGChoDanhSachVatThe( phimTruong->danhSachVatThe, phimTruong->soLuongVatThe );
      
      unsigned int soLuongGiaoDiem = 0;
      
      datLaiMangChiVatThe( phimTruong->mangChiSoVatTheSapXep, phimTruong->soLuongVatThe );
      // ==== tạo tầng bậc bao bì
      // ---- trục và tọa độ mặt phẳng đầu
      float toaDoMatPhang;
      unsigned int truc;
      tinhTrucVaToaMatPhangChia( &(phimTruong->baoBi), &truc, &toaDoMatPhang );
      
      // ---- tạo cây tầng bậc bao bì
      GiaoDiemBIH mangGiaoDiem[kSO_LUONG__GIAO_DIEM_TOI_DA];  // 28672/7 ≈ 16384 vật thể
      
      chiaVatThe( phimTruong->danhSachVatThe, phimTruong->mangChiSoVatTheSapXep, phimTruong->soLuongVatThe, 0, &(phimTruong->baoBi), truc, toaDoMatPhang, mangGiaoDiem, &soLuongGiaoDiem );
      //      xemCay( mangGiaoDiem, chiSoGiaoDiem );
      printf( "cây chiSoGiaoDiem %d\n", soLuongGiaoDiem );
      // ==== thời gian bặt đầu tính
      time_t thoiGianBatDau;
      time(&thoiGianBatDau);

      printf( "doTia: lapVongO: thongTinOChuNhat.x;y %d %d \n", thongTinOChuNhat.x, thongTinOChuNhat.y );
      // ---- thời nay chỉ hỗ trợ tính ảnh ô bằng phép chiếu phối cảnh 
      veAnhChieuPhoiCanh_O( anh, phimTruong, mangGiaoDiem, soLuongGiaoDiem, &thongTinOChuNhat );
      
      // ==== thời gian xong
      time_t thoiGianHienTai;
      time(&thoiGianHienTai);
      time_t thoiGianKetXuat = thoiGianHienTai - thoiGianBatDau;
      struct tm *hienTai = localtime( &thoiGianHienTai );

      // ---- báo số ảnh đã lưu xong và thời gian
      printf( "-->> %s <<--  %ld giây (%5.2f phút) %d/%d    %d.%02d.%02d  %02d:%02d:%02d\n", tenAnh, thoiGianKetXuat, (float)thoiGianKetXuat/60.0f,
             phimTruong->soHoatHinhDau, phimTruong->soHoatHinhCuoi, 1900 + hienTai->tm_year, hienTai->tm_mon+1, hienTai->tm_mday, hienTai->tm_hour, hienTai->tm_min, hienTai->tm_sec );
   }

   xoaThongTinOChuNhat( &thongTinOChuNhat );
}

void veAnhChieuPhoiCanh_O( Anh *anh, PhimTruong *phimTruong, GiaoDiemBIH *mangGiaoDiem, unsigned int chiSoGiaoDiem,
                          ThongTinOChuNhat *thongTinOChuNhat ) {
   
   MayQuayPhim *mayQuayPhim = &(phimTruong->mayQuayPhim);
   // ---- góc tia là vị trí máy quay phim
   Tia tia;
   tia.goc.x = mayQuayPhim->viTri.x;
   tia.goc.y = mayQuayPhim->viTri.y;
   tia.goc.z = mayQuayPhim->viTri.z;
   
   //   printf( "Tia %5.3f %5.3f %5.3f\n", tia.goc.x, tia.goc.y, tia.goc.z );
   unsigned short beRong = anh->beRong;
   unsigned short beCao = anh->beCao;
   
   //   printf( "Tia %5.3f %5.3f %5.3f\n", gocX, gocY, gocZ );
   // ---- tính vectơ hướng lên (dộc)
   Vecto huongDoc;  // hướng dọc
   huongDoc.x = mayQuayPhim->xoay[4];
   huongDoc.y = mayQuayPhim->xoay[5];
   huongDoc.z = mayQuayPhim->xoay[6];
   donViHoa( &huongDoc );
   
   Vecto huongNgang;  // hướng ngang
   huongNgang.x = -mayQuayPhim->xoay[0];
   huongNgang.y = -mayQuayPhim->xoay[1];
   huongNgang.z = -mayQuayPhim->xoay[2];
   donViHoa( &huongNgang );
   //   printf( "doTia: huongNgang %5.3f %5.3f %5.3f\n", huongNgang.x, huongNgang.y, huongNgang.z );
   //   printf( "doTia: huongDoc %5.3f %5.3f %5.3f\n\n", huongDoc.x, huongDoc.y, huongDoc.z );
   
   // ---- tính vectơ cho bước ngang
   Vecto buocNgang;
   buocNgang.x = huongNgang.x*anh->coKichDiemAnh;
   buocNgang.y = huongNgang.y*anh->coKichDiemAnh;
   buocNgang.z = huongNgang.z*anh->coKichDiemAnh;
   
   // ---- tính vectơ cho bước lên (dộc)
   Vecto buocDoc;  // bước dọc
   buocDoc.x = huongDoc.x*anh->coKichDiemAnh;
   buocDoc.y = huongDoc.y*anh->coKichDiemAnh;
   buocDoc.z = huongDoc.z*anh->coKichDiemAnh;
   //   printf( "buocgNgang %5.3f %5.3f %5.3f\n", buocNgang.x, buocNgang.y, buocNgang.z );
   //   printf( "buocDoc %5.3f %5.3f %5.3f\n\n", buocDoc.x, buocDoc.y, buocDoc.z );
   
   // ---- tính điểm góc ảnh (trái dưới)
   Vecto goc;
   goc.x = -(beCao >> 1)*buocDoc.x -(beRong >> 1)*buocNgang.x + tia.goc.x + mayQuayPhim->cachManChieu*mayQuayPhim->xoay[8];
   goc.y = -(beCao >> 1)*buocDoc.y -(beRong >> 1)*buocNgang.y + tia.goc.y + mayQuayPhim->cachManChieu*mayQuayPhim->xoay[9];
   goc.z = -(beCao >> 1)*buocDoc.z -(beRong >> 1)*buocNgang.z + tia.goc.z + mayQuayPhim->cachManChieu*mayQuayPhim->xoay[10];
   //   printf( "goc anh %5.3f %5.3f %5.3f\n", gocX, gocY, gocZ );
   //   printf( "tia.goc %5.3f %5.3f %5.3f\n", tia.goc.x, tia.goc.y, tia.goc.z );
   //   exit(0);
   
   unsigned short beRongO = thongTinOChuNhat->beRong;
   unsigned short beCaoO = thongTinOChuNhat->beCao;
   unsigned short soLuongO_ngang = thongTinOChuNhat->soLuongCot;
   unsigned short soLuongO_doc = thongTinOChuNhat->soLuongHang;

   unsigned int soLuongO = soLuongO_ngang*soLuongO_doc;

   // ---- bỏ chế độ tính tiếp cho có thể tính các ảnh sau
   if( thongTinOChuNhat->tinhTiep ) {
      thongTinOChuNhat->tinhTiep = kSAI;
   }
   else {
      thongTinOChuNhat->x = 0;
      thongTinOChuNhat->y = 0;
   }
   printf( "tính ô: %d %d\n", thongTinOChuNhat->x, thongTinOChuNhat->y );
   thongTinOChuNhat->cotBatDau = thongTinOChuNhat->x*beRongO;
   thongTinOChuNhat->hangKetThuc = anh->beCao - thongTinOChuNhat->y*beCaoO;

   unsigned short soO_ngang = thongTinOChuNhat->x;
   unsigned short soO_doc = thongTinOChuNhat->y;

   while( soO_doc < soLuongO_doc ) {

      thongTinOChuNhat->cotKetThuc = thongTinOChuNhat->cotBatDau + beRongO;

      // ---- tính hàng bắt đầu
      thongTinOChuNhat->hangBatDau = thongTinOChuNhat->hangKetThuc - beCaoO;
      printf( "  ----- thongTinOChuNhat->hangBatDau %d\n", thongTinOChuNhat->hangBatDau );
      if( thongTinOChuNhat->hangBatDau < 0 )
         thongTinOChuNhat->hangBatDau = 0;
   
      if( thongTinOChuNhat->hangKetThuc > anh->beCao )
         thongTinOChuNhat->hangKetThuc = anh->beCao;

      while( soO_ngang < soLuongO_ngang ) {
         printf( "tính ô: %d %d   %3d/%d   (%d; %d) (%d; %d)\n", soO_ngang, soO_doc, soLuongO_ngang*soO_doc + soO_ngang, soLuongO, thongTinOChuNhat->cotBatDau, thongTinOChuNhat->hangBatDau, thongTinOChuNhat->cotKetThuc, thongTinOChuNhat->hangKetThuc );
         
         // ---- tính cột kết thúc
         thongTinOChuNhat->cotKetThuc = thongTinOChuNhat->cotBatDau + beRongO;
         if( thongTinOChuNhat->cotKetThuc > anh->beRong )
            thongTinOChuNhat->cotKetThuc = anh->beRong;
         
         tinhO( thongTinOChuNhat, phimTruong, mangGiaoDiem, chiSoGiaoDiem,
                &tia, &goc, &huongDoc, &huongNgang, &buocNgang, &buocDoc );
         
         // ---- lưu ảnh
         luuOChuNhatZIP( anh->ten, thongTinOChuNhat, kKIEU_HALF );

         thongTinOChuNhat->x++;
         soO_ngang++;
         thongTinOChuNhat->cotBatDau += beRongO;
         thongTinOChuNhat->cotKetThuc += beRongO;
         if( thongTinOChuNhat->cotKetThuc > anh->beRong )
            thongTinOChuNhat->cotKetThuc = anh->beRong;
      }
      soO_doc++;
      
      // ---- trở lại đầu hàng, pha ̉i làm ở đây để khởi tính ảnh kl
      soO_ngang = 0;
      thongTinOChuNhat->cotBatDau = 0;
      thongTinOChuNhat->x = 0;

      thongTinOChuNhat->hangKetThuc -= beCaoO;

      thongTinOChuNhat->y++;
   }
   
}


void tinhO( ThongTinOChuNhat *thongTinOChuNhat, PhimTruong *phimTruong, GiaoDiemBIH *mangGiaoDiem, unsigned int chiSoGiaoDiem, Tia *tia, Vecto *goc, Vecto *huongDoc, Vecto *huongNgang, Vecto *buocNgang, Vecto *buocDoc ) {
   
   short hang = thongTinOChuNhat->hangKetThuc;
   short hangBatDau = thongTinOChuNhat->hangBatDau;
   unsigned short hangKetThuc = thongTinOChuNhat->hangKetThuc;
   unsigned int chiSoAnh = 0;
   printf("tinhO: %d %d\n", hangBatDau, hang );

   while( hang > hangBatDau ) {
      printf( "  hàng %3d/%d   (%d‰)\n", hang - hangBatDau, hangKetThuc - hangBatDau, 1000*(hang - hangBatDau)/(hangKetThuc - hangBatDau) );
      // ---- tính hướng cho tia của điểm ảnh này
      tia->huong.x = goc->x + buocDoc->x*hang + buocNgang->x*thongTinOChuNhat->cotBatDau - tia->goc.x;
      tia->huong.y = goc->y + buocDoc->y*hang + buocNgang->y*thongTinOChuNhat->cotBatDau - tia->goc.y;
      tia->huong.z = goc->z + buocDoc->z*hang + buocNgang->z*thongTinOChuNhat->cotBatDau - tia->goc.z;

      short cot = thongTinOChuNhat->cotBatDau;
      short cotKetThuc = thongTinOChuNhat->cotKetThuc;

      while( cot < cotKetThuc ) {
         //        printf( "  cot %d  chiSoAnh %d", cot, chiSoAnh );
         Mau mauDoTia = doTia( tia, phimTruong, mangGiaoDiem, chiSoGiaoDiem, 0 );
         Mau mauBauTroi = hoaTietBauTroi( tia->huong, &(phimTruong->hoaTietBauTroi) );
         Mau mauCoSuongMu = toSuongMu( &(tia->huong), &mauDoTia, &mauBauTroi );
      
         // ---- giữ điểm ảnh kết xuất
         thongTinOChuNhat->kenhDo[chiSoAnh] = mauCoSuongMu.d;
         thongTinOChuNhat->kenhLuc[chiSoAnh] = mauCoSuongMu.l;
         thongTinOChuNhat->kenhXanh[chiSoAnh] = mauCoSuongMu.x;
         thongTinOChuNhat->kenhDuc[chiSoAnh] = mauDoTia.dd;
         thongTinOChuNhat->kenhXa[chiSoAnh] = mauDoTia.c;

         // ---- hướng tia tiếp
         tia->huong.x += buocNgang->x;
         tia->huong.y += buocNgang->y;
         tia->huong.z += buocNgang->z;
         
         cot++;
         chiSoAnh++;
      }

      hang--;
   }

}

#pragma mark ---- Khởi Động Lại
unsigned char docXauTuTep( FILE *tep, char *xau ) {
   
   unsigned char chiSo = 0;
   do {
      xau[chiSo] = fgetc( tep );
      chiSo++;
   } while( xau[chiSo-1] != 0x00 );
   
   return chiSo-1;
}

#include <string.h>
unsigned char timTepVaThongTinO_TinhTiep( char *tenTep, ThongTinOChuNhat *thongTinOChuNhat, Anh *anh ) {

   FILE *tep = fopen( tenTep, "r" );
   
   if( tep != NULL ) {
      // ---- nhảy qua 5 byte cho đầu tệp
      fseek( tep, 8, SEEK_SET );
      
      unsigned int beRongO = kKICH_THUOC_O_CHU_NHTA_MAC_DINH;
      unsigned int beCaoO = kKICH_THUOC_O_CHU_NHTA_MAC_DINH;
      unsigned int beRongAnh;
      unsigned int beCaoAnh;

      // ==== tìm kích thước ô
      unsigned char xong = 0;
      
      do {
         // ---- đọc tên (xâu) đặc điểm
         char tenDacDiem[255];
         unsigned char beDaiXau = docXauTuTep( tep, tenDacDiem );
         
         if( beDaiXau ) {
            // ---- đọc kiểu dữ liệu
            char loaiDuLieuDacDiem[255];
            docXauTuTep( tep, loaiDuLieuDacDiem );
            
            // ---- đọc bề dài dữ liệu đặc điểm
            unsigned int beDaiDacDiem = 0;
            fread( &beDaiDacDiem, 4, 1, tep );
            //         printf( "  %x %s %s %d\n", tenDacDiem[0], tenDacDiem, loaiDuLieuDacDiem, beDaiDacDiem );

            if( !strcmp( "dataWindow", tenDacDiem ) ) {
               unsigned int cuaSoTrai;
               unsigned int cuaSoPhai;
               unsigned int cuaSoDuoi;
               unsigned int cuaSoTren;
               fread( &cuaSoTrai, 4, 1, tep );
               fread( &cuaSoDuoi, 4, 1, tep );
               fread( &cuaSoPhai, 4, 1, tep );
               fread( &cuaSoTren, 4, 1, tep );
               beRongAnh = cuaSoPhai - cuaSoTrai + 1;
               beCaoAnh = cuaSoTren - cuaSoDuoi + 1;
            }
            else if( !strcmp( "tiles", tenDacDiem) ) {
               
               fread( &beRongO, 4, 1, tep );
               fread( &beCaoO, 4, 1, tep );
               
               // ---- bỏ byte mức ô
               fgetc( tep );
            }
            else
               // ---- bỏ qua
               fseek( tep, ftell( tep ) + beDaiDacDiem, SEEK_SET );
            
            //        printf( "%s %s %d\n", attribute_name, attribute_data_type, attribute_length );
         }
         else
            xong = 0x01;
      } while( !xong );
      
      datThongTinOChuNhat( thongTinOChuNhat, beRongO, beCaoO, beRongAnh, beCaoAnh );
 
      anh->beRong = beRongAnh;
      anh->beCao = beCaoAnh;

      // ==== tìm số ô chưa tính
      unsigned long long dauBanViTriThanhPhan = ftell( tep );
      unsigned int soLuongO_cao = thongTinOChuNhat->soLuongHang;
      unsigned int soLuongO_ngang = thongTinOChuNhat->soLuongCot;
      
      unsigned short soO_cao = 0;
      unsigned int soO = 0;
      
      while( soO_cao < soLuongO_cao ) {
         
         unsigned int soO_ngang = 0;
         while( soO_ngang < soLuongO_ngang ) {
            
            unsigned long long diaChiTrongTep;
            fread( &diaChiTrongTep, 8, 1, tep );
            
            if( diaChiTrongTep == 0 ) {
               thongTinOChuNhat->x = soO_ngang;
               thongTinOChuNhat->y = soO_cao;
               thongTinOChuNhat->viTriTrongBangThanhPhan = diaChiTrongTep;
               
               // ---- tìm được rồi, thoạt hai lặp vòng
               soO_ngang = soLuongO_ngang;
               soO_cao = soLuongO_cao;
            }
            
            soO++;
            soO_ngang++;
         }
         soO_cao++;
      }
   
      thongTinOChuNhat->viTriTrongBangThanhPhan = dauBanViTriThanhPhan + ((thongTinOChuNhat->y*soLuongO_ngang + thongTinOChuNhat->x) << 3);
      printf( "Khởi động lại - kích thước ảnh %d %d\n  số ô khởi động: %d %d  kích thước ô: %d %d\n  viTriTongBangThanhPhan %ld\n", anh->beRong, anh->beCao, thongTinOChuNhat->x, thongTinOChuNhat->y,
             thongTinOChuNhat->beRong, thongTinOChuNhat->beCao, thongTinOChuNhat->viTriTrongBangThanhPhan );
      
      fclose( tep );
   }
   else {
      printf( "timThongTinOTinhTiep: Vấn đề mở tệp, có lẻ tệp %s không tồn tại.\n", tenTep );
      return kSAI;   // không có tệp
   }
   
   return kDUNG; // có tệp

}

#pragma mark ---- DO TÌA
#define kSO_LUONG__VAT_THE_BONG_TOI_DA 16

Mau doTia( Tia *tia, PhimTruong *phimTruong, GiaoDiemBIH *mangGiaoDiem, unsigned int chiSoGiaoDiem, unsigned short soNhoi ) {
   
   Mau mau;
   mau.d = 0.0f;
   mau.l = 0.0f;
   mau.x = 0.0f;
   mau.dd = 1.0f;
   mau.c = kVO_CUC;
   
   float cachXaGanNhat = kVO_CUC;   // ---- đặt cáchXaGầnNhất = kVÔ_HẠN
   
   // ==== xem nếu tia có trúng vật thể nào
   unsigned short chiSoVat = 0; // chỉ số vật thể
   ThongTinToMau thongTinToMauVatTheGanNhat;
   thongTinToMauVatTheGanNhat.vatThe = NULL;  // chưa có vật thể gần nhất
   
   xemTiaCoTrungVatTheGanNhat( mangGiaoDiem, chiSoGiaoDiem, phimTruong->danhSachVatThe, phimTruong->mangChiSoVatTheSapXep, tia, &thongTinToMauVatTheGanNhat );
   
   // ==== tính màu
   if( thongTinToMauVatTheGanNhat.vatThe != NULL ) {  // tô màu
      
      donViHoa( &(thongTinToMauVatTheGanNhat.phapTuyenTDVT) );
      float cachXa = thongTinToMauVatTheGanNhat.cachXa;
      // ---- điểm trúng (tọa độ thế giới)
      Vecto diemTrung;
      diemTrung.x = tia->goc.x + cachXa*tia->huong.x;
      diemTrung.y = tia->goc.y + cachXa*tia->huong.y;
      diemTrung.z = tia->goc.z + cachXa*tia->huong.z;
      
      // ---- tính cách xa
      if( soNhoi == 0 ) {
         // ---- tính điểm trúng tương đối với máy quay phim
         Vecto viTriMayQuayPhim = phimTruong->mayQuayPhim.viTri;
         Vecto diemTrungTuongDoi;
         diemTrungTuongDoi.x = diemTrung.x - viTriMayQuayPhim.x;
         diemTrungTuongDoi.y = diemTrung.y - viTriMayQuayPhim.y;
         diemTrungTuongDoi.z = diemTrung.z - viTriMayQuayPhim.z;
         //         printf( "doTia: %5.3f %5.3f %5.3f\n", diemTrungTuongDoi.x, diemTrungTuongDoi.y, diemTrungTuongDoi.z );
         mau.c = sqrtf( diemTrungTuongDoi.x*diemTrungTuongDoi.x + diemTrungTuongDoi.y*diemTrungTuongDoi.y + diemTrungTuongDoi.z*diemTrungTuongDoi.z );
      }
      
      // ---- điểm trúng cho nguồn ánh sáng gần nhất. Cần cộng thêm pháp tuyến môt chút cho chắc chắn ở ngoài vật thể
//      printf( " doTia: thongTinToMauVatTheGanNhat.cachXa %5.3f\n", thongTinToMauVatTheGanNhat.cachXa );
      Vecto diemTrungChoThayNAS; // điểm trúng cho thấy nguồn ánh sáng
      diemTrungChoThayNAS.x = diemTrung.x + 0.005f*thongTinToMauVatTheGanNhat.phapTuyenTDVT.x*cachXa;
      diemTrungChoThayNAS.y = diemTrung.y + 0.005f*thongTinToMauVatTheGanNhat.phapTuyenTDVT.y*cachXa;
      diemTrungChoThayNAS.z = diemTrung.z + 0.005f*thongTinToMauVatTheGanNhat.phapTuyenTDVT.z*cachXa;
      
      //      printf( "diemTrungChoThayNAS %5.3f %5.3f %5.3f\n", diemTrungChoThayNAS.x, diemTrungChoThayNAS.y, diemTrungChoThayNAS.z );
      // ---- xem nếu được thấy nguồn ánh sáng cho bóng tối (chỉ có mặt trời cách xa vô cực)
      ThongTinToMau thongTinToMauBong[kSO_LUONG__VAT_THE_BONG_TOI_DA];
      unsigned char soLuongVatTheBongToi = 0;
      unsigned char thay = thayNguonAnhSang( phimTruong->danhSachVatThe, phimTruong->mangChiSoVatTheSapXep, phimTruong->soLuongVatThe, &diemTrungChoThayNAS, &(phimTruong->matTroi.huongAnh), thongTinToMauBong, &soLuongVatTheBongToi,mangGiaoDiem, chiSoGiaoDiem );

      // ---- tính màu nguồn ánh sáng
      Mau mauNguonAnhSang;
      if( thay ) {
         mauNguonAnhSang.d = phimTruong->matTroi.mauAnh.d;
         mauNguonAnhSang.l = phimTruong->matTroi.mauAnh.l;
         mauNguonAnhSang.x = phimTruong->matTroi.mauAnh.x;
         mauNguonAnhSang.dd = phimTruong->matTroi.mauAnh.dd;
      }
      else {
         //         printf( "soLuongVatTheBongToi %d\n", soLuongVatTheBongToi );
         // ---- tịnh độ đục từ hết vật thể
         float duc = 0.0f;
         unsigned char soVatTheBongToi = 0;
         while( soVatTheBongToi < soLuongVatTheBongToi ) {
            //         printf("thongTinToMauBong loaiVatThe %d\n", thongTinToMauBong.vatThe->loai );
            // ---- tìm đục vật thể
            Vecto toaDoHoaTiet = nhanVectoVoiMaTran3x3( &(thongTinToMauBong[soVatTheBongToi].diemTrungTDVT), thongTinToMauBong[soVatTheBongToi].vatThe->phongTo ); // <---- TẠI SAO .phongTo??? phóng to họa tiết
            // toMauVat( VatThe *vatThe, Vecto *diemTrungTDVT, Vecto phapTuyen, Vecto huongTia )
            Mau mauVat = toMauVat( thongTinToMauBong[soVatTheBongToi].vatThe, &toaDoHoaTiet, thongTinToMauBong[soVatTheBongToi].phapTuyenTDVT, phimTruong->matTroi.huongAnh );
            //            printf( "mauVat.dd %5.3f  duc %5.3f\n", mauVat.dd, duc );
            if( mauVat.dd > duc )
               duc = mauVat.dd;
            soVatTheBongToi++;
         }
         /*        if( (mauVat.dd < 0.0f) || (mauVat.dd > 1.0f) ) {
          printf( "doTia: thongTinToMau.phapTuyen %5.3f %5.3f %5.3f\n", thongTinToMauBong.phapTuyenTDVT.x, thongTinToMauBong.phapTuyenTDVT.y, thongTinToMauBong.phapTuyenTDVT.z );
          printf( "doTia: thongTinToMauBong.vatThe %d  mauVat.dd %5.3f\n", thongTinToMauBong.vatThe->loai, mauVat.dd );
          exit(0);
          }*/
         
         // ---- vật thể đục
         if( duc == 1.0f ) {
            mauNguonAnhSang.d = 0.1f;  // màu ánh sánh phản xạ từ môi trường
            mauNguonAnhSang.l = 0.1f;
            mauNguonAnhSang.x = 0.2f;
            mauNguonAnhSang.dd = 1.0f;
         }
         else {
            float nghichDuc = 1.0f - duc;
            
            mauNguonAnhSang.d = 0.1f + 0.9f*(phimTruong->matTroi.mauAnh.d*nghichDuc);
            mauNguonAnhSang.l = 0.1f + 0.9f*(phimTruong->matTroi.mauAnh.l*nghichDuc);
            mauNguonAnhSang.x = 0.2f + 0.8f*(phimTruong->matTroi.mauAnh.x*nghichDuc);
            mauNguonAnhSang.dd = 1.0f;
         }
         
      }
      
      // ---- kiếm màu cho vật thể
      Vecto toaDoHoaTiet = nhanVectoVoiMaTran3x3( &(thongTinToMauVatTheGanNhat.diemTrungTDVT), thongTinToMauVatTheGanNhat.vatThe->phongTo ); // <---- TẠI SAO .phongTo???
      
      Mau mauVat = toMauVat( thongTinToMauVatTheGanNhat.vatThe, &toaDoHoaTiet, thongTinToMauVatTheGanNhat.phapTuyenTDVT, tia->huong );
      Mau mauTanXa = tinhMauTanXa( &mauNguonAnhSang, &mauVat, &(phimTruong->matTroi.huongAnh), &(thongTinToMauVatTheGanNhat.phapTuyenTDVT) );
      
      if( soNhoi + 1 < phimTruong->soNhoiToiDa ) {   // dưới giới hạn nhồi chưa
         float tiSoPhanXa = mauVat.p;
         float doDuc = mauVat.dd;
         
         Mau mauPhanXa;
         Mau mauCaoQuang;
         if( tiSoPhanXa ) {
            // ---- tia phản xạ
            Tia tiaPhanXa = tinhTiaPhanXa( thongTinToMauVatTheGanNhat.phapTuyenTDVT, diemTrung, tia->huong );
            mauPhanXa = doTia( &tiaPhanXa, phimTruong, mangGiaoDiem, chiSoGiaoDiem, soNhoi + 1 );
            // ---- tính màu cao quang
            if( thay || (mauVat.dd < 1.0f) ) {
               mauCaoQuang = tinhCaoQuang( &(phimTruong->matTroi), &(tiaPhanXa.huong), 250.0f*tiSoPhanXa );
               float chinh = sqrtf(tiSoPhanXa);
               mauCaoQuang.d *= chinh;  // màu ánh sánh phản xạ từ môi trường
               mauCaoQuang.l *= chinh;
               mauCaoQuang.x *= chinh;
               //  mauCaoQuang.dd = 1.0f;
            }
            else {
               mauCaoQuang.d = 0.0f;  // màu ánh sánh phản xạ từ môi trường
               mauCaoQuang.l = 0.0f;
               mauCaoQuang.x = 0.0f;
               mauCaoQuang.dd = 1.0f;
            }
         }
         else {
            mauPhanXa.d = 0.0f;
            mauPhanXa.l = 0.0f;
            mauPhanXa.x = 0.0f;
            mauPhanXa.dd = 0.0f;
            mauCaoQuang.d = 0.0f;
            mauCaoQuang.l = 0.0f;
            mauCaoQuang.x = 0.0f;
            mauCaoQuang.dd = 0.0f;
         }
         
         Mau mauKhucXa;
         // ---- khúc xạ
         if( doDuc < 1.0f ) {
            // ---- tia khúc xạ
            Tia tiaKhucXa = tinhTiaKhucXa( thongTinToMauVatTheGanNhat.phapTuyenTDVT, diemTrung, tia->huong, thongTinToMauVatTheGanNhat.vatThe->chietSuat );
            mauKhucXa = doTia( &tiaKhucXa, phimTruong, mangGiaoDiem, chiSoGiaoDiem, soNhoi + 1 );
         }
         else {
            mauKhucXa.d = 0.0f;
            mauKhucXa.l = 0.0f;
            mauKhucXa.x = 0.0f;
            mauKhucXa.dd = 0.0f;
         }
         
         // ---- gồm màu
         if( doDuc == 1.0f ) {
            if( tiSoPhanXa == 0.0f ) {
               mau.d = mauTanXa.d;
               mau.l = mauTanXa.l;
               mau.x = mauTanXa.x;
               mau.dd = mauTanXa.dd;
            }
            else {
               float nghichTiSoPhanXa = 1.0f - tiSoPhanXa;
               mau.d = nghichTiSoPhanXa*mauTanXa.d + tiSoPhanXa*mauPhanXa.d + mauCaoQuang.d;
               mau.l = nghichTiSoPhanXa*mauTanXa.l + tiSoPhanXa*mauPhanXa.l + mauCaoQuang.l;
               mau.x = nghichTiSoPhanXa*mauTanXa.x + tiSoPhanXa*mauPhanXa.x + mauCaoQuang.x;
               mau.dd = nghichTiSoPhanXa*mauTanXa.dd + tiSoPhanXa*mauPhanXa.dd + mauCaoQuang.dd;
            }
         }
         else {
            float nghichDoDuc = 1.0f - doDuc;
            if( tiSoPhanXa == 0.0f ) {
               mau.d = doDuc*mauTanXa.d + nghichDoDuc*mauKhucXa.d;
               mau.l = doDuc*mauTanXa.l + nghichDoDuc*mauKhucXa.l;
               mau.x = doDuc*mauTanXa.x + nghichDoDuc*mauKhucXa.x;
               mau.dd = doDuc*mauTanXa.dd + nghichDoDuc*mauKhucXa.dd;
            }
            else {
               float nghichTiSoPhanXa = 1.0f - tiSoPhanXa;
               mau.d = nghichTiSoPhanXa*(doDuc*mauTanXa.d + nghichDoDuc*mauKhucXa.d) + tiSoPhanXa*mauPhanXa.d + mauCaoQuang.d;
               mau.l = nghichTiSoPhanXa*(doDuc*mauTanXa.l + nghichDoDuc*mauKhucXa.l) + tiSoPhanXa*mauPhanXa.l + mauCaoQuang.l;
               mau.x = nghichTiSoPhanXa*(doDuc*mauTanXa.x + nghichDoDuc*mauKhucXa.x) + tiSoPhanXa*mauPhanXa.x + mauCaoQuang.x;
               mau.dd = nghichTiSoPhanXa*(doDuc*mauTanXa.dd + nghichDoDuc*mauKhucXa.dd) + tiSoPhanXa*mauPhanXa.dd + mauCaoQuang.dd;
            }
         }
         
      }
      else {  // đến giới hạn rồi, cho màu tán xạ
         // ---- xài màu tán xạ
         mau.d = mauTanXa.d;
         mau.l = mauTanXa.l;
         mau.x = mauTanXa.x;
         mau.dd = mauTanXa.dd;
      }
   }
   else {
      // ---- tính màu bầu trời
      mau = hoaTietBauTroi( tia->huong, &(phimTruong->hoaTietBauTroi) );
      // ---- đặt cách xa
      if( soNhoi == 0 )
         mau.c = kVO_CUC;
   }
   
   return mau;
}
