// Quaternion.h

#pragma once
#include "Vecto.h"

typedef struct {
   float w;
   float x;
   float y;
   float z;
} Quaternion;


/* Đặt quaternion cho vectơ và góc quay */
Quaternion datQuaternionTuVectoVaGocQuay( Vecto *vecto, float gocQuay );

/* quaternion sang ma trận */
void quaternionSangMaTran( Quaternion *quaternion, float *maTran );

/* nhân quaternion với quaternion */
Quaternion nhanQuaternionVoiQuaternion( Quaternion *quaternion0, Quaternion *quaternion1 );

/* tính xoay cho vật thể */
Quaternion tinhXoayChoVatThe( Quaternion *quaternionVatThe, Vecto trucXoay, float tocDo, float banKinh );