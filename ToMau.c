#include "ToMau.h"
#include "HoaTiet/HoaTietTraiBanh.h"

#pragma mark ---- Tô Màu Hoạ Tiết
Mau toMauVat( VatThe *vatThe, Vecto *diemTrungTDVT, Vecto phapTuyen, Vecto huongTia ) {
   
   //   printf( "  toMauVat: phapTuyen %5.3f %5.3f %5.3f\n", phapTuyen.x, phapTuyen.y, phapTuyen.z );
   // ---- điểm trúng biến hóa rồi (tọa độ vật thể)
   Mau mauVat;
   if( vatThe->soHoaTiet == kHOA_TIET__KHONG )
      mauVat = vatThe->hoaTiet.hoaTietKhong.mau;
   
   else if( vatThe->soHoaTiet == kHOA_TIET__DI_HUONG )
      mauVat = hoaTietDiHuong( phapTuyen, huongTia, &(vatThe->hoaTiet.hoaTietDiHuong) );
   
   else if( vatThe->soHoaTiet == kHOA_TIET__CA_RO )
      mauVat = hoaTietCaRo( diemTrungTDVT, &(vatThe->hoaTiet.hoaTietCaRo) );

   else if( vatThe->soHoaTiet == kHOA_TIET__CA_RO_SONG )
      mauVat = hoaTietCaRoSong( diemTrungTDVT, &(vatThe->hoaTiet.hoaTietCaRoSong) );
   
   else if( vatThe->soHoaTiet == kHOA_TIET__CA_RO_SAU_MAU )
      mauVat = hoaTietCaRoSauMau( diemTrungTDVT, &(vatThe->hoaTiet.hoaTietCaRoSauMau) );

   else if( vatThe->soHoaTiet == kHOA_TIET__VONG_TRON )
      mauVat = hoaTietVongTron( diemTrungTDVT, &(vatThe->hoaTiet.hoaTietVongTron) );
   
   else if( vatThe->soHoaTiet == kHOA_TIET__OC_XOAY_2 )
      mauVat = hoaTietOcXoay2( diemTrungTDVT, &(vatThe->hoaTiet.hoaTietOcXoay2) );
   
   else if( vatThe->soHoaTiet == kHOA_TIET__OC_XOAY_3 )
      mauVat = hoaTietOcXoay3( diemTrungTDVT, &(vatThe->hoaTiet.hoaTietOcXoay3) );
   
   else if( vatThe->soHoaTiet == kHOA_TIET__TRAI_BANH )
      
      mauVat = hoaTietTraiBanh( diemTrungTDVT );
   
   else if( vatThe->soHoaTiet == kHOA_TIET__GAN )
      mauVat = hoaTietGan( diemTrungTDVT->x, &(vatThe->hoaTiet.hoaTietGan) );
   
   else if( vatThe->soHoaTiet == kHOA_TIET__CHAM_BI )
      mauVat = hoaTietChamBi( diemTrungTDVT, &(vatThe->hoaTiet.hoaTietChamBi) );
   
   else if( vatThe->soHoaTiet == kHOA_TIET__NGOI_SAO_CAU )
      mauVat = hoaTietNgoiSaoCau( diemTrungTDVT, &(vatThe->hoaTiet.hoaTietNgoiSaoCau) );
   
   else if( vatThe->soHoaTiet == kHOA_TIET__QUAN )
      mauVat = hoaTietQuan( diemTrungTDVT, &(vatThe->hoaTiet.hoaTietQuan) );
   
   else if( vatThe->soHoaTiet == kHOA_TIET__QUAN_XOAY )
      mauVat = hoaTietQuanXoay( diemTrungTDVT, &(vatThe->hoaTiet.hoaTietQuanXoay) );
   
   else if( vatThe->soHoaTiet == kHOA_TIET__QUAN_SONG_THEO_HUONG )
      mauVat = hoaTietQuanSongTheoHuong( diemTrungTDVT, &(vatThe->hoaTiet.hoaTietQuanSongTheoHuong) );
   
   else if( vatThe->soHoaTiet == kHOA_TIET__QUAN_SONG_TRUC_Z )
      mauVat = hoaTietQuanSongTrucZ( diemTrungTDVT, &(vatThe->hoaTiet.hoaTietQuanSongTrucZ) );
   
   else if( vatThe->soHoaTiet == kHOA_TIET__QUAN_SONG_TIA_PHAI )
      mauVat = hoaTietQuanSongTiaPhai( diemTrungTDVT, &(vatThe->hoaTiet.hoaTietQuanSongTiaPhai) );
   
   else if( vatThe->soHoaTiet == kHOA_TIET__SOC )
      mauVat = hoaTietSoc( diemTrungTDVT, &(vatThe->hoaTiet.hoaTietSoc) );
   
   else if( vatThe->soHoaTiet == kHOA_TIET__CA_RO_MIN )
      mauVat = hoaTietCaRoMin( diemTrungTDVT, &(vatThe->hoaTiet.hoaTietCaRoMin) );
   
   else if( vatThe->soHoaTiet == kHOA_TIET__HAI_CHAM_BI )
      mauVat = hoaTietHaiChamBi( diemTrungTDVT, &(vatThe->hoaTiet.hoaTietHaiChamBi) );
   
   else if( vatThe->soHoaTiet == kHOA_TIET__BONG_VONG )
      mauVat = hoaTietBongVong( diemTrungTDVT, &(vatThe->hoaTiet.hoaTietBongVong) );

   else if( vatThe->soHoaTiet == kHOA_TIET__KIM_TUYEN_LAP_LANH )
      mauVat = hoaTietKimTuyenLapLanh( &(vatThe->hoaTiet.hoaTietKimTuyenLapLanh) );

   else
      printf( "ToMâuVật: không biết họa tiết này %d\n", vatThe->soHoaTiet );
   //   printf( "toMauVat: phapTuyen %5.3f %5.3f %5.3f   mauVat.dd %5.3f\n", phapTuyen.x, phapTuyen.y, phapTuyen.z, mauVat.dd );
   return mauVat;
}


Mau tinhMauTroi( Vecto *huongTia ) {
   
   Mau mauChanTroi;   // màu chân trời
   mauChanTroi.d = 0.7f;
   mauChanTroi.l = 0.7f;
   mauChanTroi.x = 1.0f;
   mauChanTroi.dd = 1.0f;

   Mau mauThienDinh;   // màu thiên đỉnh
   mauThienDinh.d = 0.0f;
   mauThienDinh.l = 0.0f;
   mauThienDinh.x = 0.5f;
   mauThienDinh.dd = 1.0f;
   
//   Mau mauThienDay;   // màu thiên đấy
//   mauThienDay.d = 0.5f;
//   mauThienDay.l = 0.2f;
//   mauThienDay.x = 0.0f;
//   mauThienDay.dd = 1.0f;
   
   // ---- chép hướng tia, không muốn đơn vị họa hướngTia gốc
   Vecto huongTiaDVH;  // hướngTiaĐơnVịHóa 
   huongTiaDVH.x = huongTia->x;
   huongTiaDVH.y = huongTia->y;
   huongTiaDVH.z = huongTia->z;

   donViHoa( &huongTiaDVH );

   Mau mauTroi;
   if( huongTiaDVH.y < 0.0f ) {
      mauTroi.d = mauChanTroi.d;
      mauTroi.l = mauChanTroi.l;
      mauTroi.x = mauChanTroi.x;
      mauTroi.dd = mauChanTroi.dd;
   }
   else {   // bầu trời
      float tiSoChanTroi = 1.0f - huongTiaDVH.y;
//      printf( "huongY %5.3f  tiSoChanTroi %5.3f\n", huongTiaDVH.y, tiSoChanTroi );
      tiSoChanTroi = powf( tiSoChanTroi, 8.0f );
//      printf( "    tiSoChanTroi %5.3f\n", tiSoChanTroi );
      mauTroi.d = mauChanTroi.d*tiSoChanTroi + mauThienDinh.d*(1.0f - tiSoChanTroi);
      mauTroi.l = mauChanTroi.l*tiSoChanTroi + mauThienDinh.l*(1.0f - tiSoChanTroi);
      mauTroi.x = mauChanTroi.x*tiSoChanTroi + mauThienDinh.x*(1.0f - tiSoChanTroi);
      mauTroi.dd = mauChanTroi.dd*tiSoChanTroi + mauThienDinh.dd*(1.0f - tiSoChanTroi);
   }

   return mauTroi;
}


Mau tinhMauTanXa( Mau *mauMatTroi, Mau *mauVat, Vecto *huongNguonAnhSang, Vecto *phapTuyen ) {
   
   // ---- tính độ sáng tử mặt
   float tichVoHuong = huongNguonAnhSang->x*phapTuyen->x + huongNguonAnhSang->y*phapTuyen->y + huongNguonAnhSang->z*phapTuyen->z;

   // ---- ánh sáng trúng mặt có hướng nghịch chiếu với pháp tuyến
   tichVoHuong = -tichVoHuong;
   
   if( tichVoHuong < 0.0f )
      tichVoHuong = 0.0f;  // cho một chút ánh sáng để giả bộ ánh sáng từ môi trường

   Mau mauTanXa;
   mauTanXa.d = tichVoHuong*(mauVat->d);
   mauTanXa.l = tichVoHuong*(mauVat->l);
   mauTanXa.x = tichVoHuong*(mauVat->x);
   mauTanXa.dd = tichVoHuong*(mauVat->dd);
   
   mauTanXa.d *= mauMatTroi->d;
   mauTanXa.l *= mauMatTroi->l;
   mauTanXa.x *= mauMatTroi->x;
//   mauTanXa.dd *= 1.0f;//mauMatTroi->dd;
   
   mauTanXa.d += 0.05f*mauVat->d;
   mauTanXa.l += 0.05f*mauVat->l;
   mauTanXa.x += 0.05f*mauVat->x;
   return mauTanXa;
}

Tia tinhTiaPhanXa( Vecto phapTuyen, Vecto diemTrung, Vecto huongTrung ) {
   
   Tia tiaPhanXa;

   // ---- tích vô hướng giữa hướng tia gốc với vectơ vuông góc
   donViHoa( &huongTrung );
   float tichVoHuong = phapTuyen.x*huongTrung.x + phapTuyen.y*huongTrung.y + phapTuyen.z*huongTrung.z;

   // ---- tính hướng tia phản xạ
   tiaPhanXa.huong.x = huongTrung.x - 2.0f*tichVoHuong*phapTuyen.x;
   tiaPhanXa.huong.y = huongTrung.y - 2.0f*tichVoHuong*phapTuyen.y;
   tiaPhanXa.huong.z = huongTrung.z - 2.0f*tichVoHuong*phapTuyen.z;
   
   // ---- cộng thêm một chút cho ra vật thể
   tiaPhanXa.goc.x = diemTrung.x + 0.001f*tiaPhanXa.huong.x;
   tiaPhanXa.goc.y = diemTrung.y + 0.001f*tiaPhanXa.huong.y;
   tiaPhanXa.goc.z = diemTrung.z + 0.001f*tiaPhanXa.huong.z;

   return tiaPhanXa;
}

Tia tinhTiaKhucXa( Vecto phapTuyen, Vecto diemTrung, Vecto huongTrung, float chietSuat ) {

   Tia tiaKhucXa;

   // ---- tia trúng cùng hướng với pháp tuyến cho biết hướng khúc xạ
   donViHoa( &huongTrung );
   float tichVoHuong = phapTuyen.x*huongTrung.x + phapTuyen.y*huongTrung.y + phapTuyen.z*huongTrung.z;

   if( tichVoHuong < 0.0f ) { // vào vật thể
      float tiSoXuyen = 1.0f/chietSuat; // sin θ2 - tỉ số chiết suất
      float a = 1.0f - tichVoHuong*tichVoHuong;   // nhớ tích vô hướng < 0.0
      float b = sqrtf( 1.0f - tiSoXuyen*tiSoXuyen*a );  // cos θ2
      
      tiaKhucXa.huong.x = (huongTrung.x - phapTuyen.x*tichVoHuong)*tiSoXuyen - phapTuyen.x*b;
      tiaKhucXa.huong.y = (huongTrung.y - phapTuyen.y*tichVoHuong)*tiSoXuyen - phapTuyen.y*b;
      tiaKhucXa.huong.z = (huongTrung.z - phapTuyen.z*tichVoHuong)*tiSoXuyen - phapTuyen.z*b;

      // ---- cộng thêm một chút cho vào vật thể
      tiaKhucXa.goc.x = diemTrung.x + 0.001f*tiaKhucXa.huong.x;
      tiaKhucXa.goc.y = diemTrung.y + 0.001f*tiaKhucXa.huong.y;
      tiaKhucXa.goc.z = diemTrung.z + 0.001f*tiaKhucXa.huong.z;
   }
   else { // ra chất liệu
      float tiSoXuyen = chietSuat/1.0f;   // sin θ2 - tỉ số chiết suất
      float a = 1.0f - tichVoHuong*tichVoHuong;   // nhớ tích vô hướng < 0.0
      float b = 1.0f - tiSoXuyen*tiSoXuyen*a;  // cos θ2
      if( b < 0.0f ) {  // θ2 > 90º, không khúc xạ, phản xạ
         tiaKhucXa = tinhTiaPhanXa( phapTuyen, diemTrung, huongTrung );
      }
      else {
         b = sqrtf(b);
         tiaKhucXa.huong.x = (huongTrung.x - phapTuyen.x*tichVoHuong)*tiSoXuyen + phapTuyen.x*b;
         tiaKhucXa.huong.y = (huongTrung.y - phapTuyen.y*tichVoHuong)*tiSoXuyen + phapTuyen.y*b;
         tiaKhucXa.huong.z = (huongTrung.z - phapTuyen.z*tichVoHuong)*tiSoXuyen + phapTuyen.z*b;

         // ---- cộng thêm một chút cho ra vật thể
         tiaKhucXa.goc.x = diemTrung.x + 0.001f*tiaKhucXa.huong.x;
         tiaKhucXa.goc.y = diemTrung.y + 0.001f*tiaKhucXa.huong.y;
         tiaKhucXa.goc.z = diemTrung.z + 0.001f*tiaKhucXa.huong.z;
      }
   }


   return tiaKhucXa;
}

Mau tinhCaoQuang( MatTroi *matTroi, Vecto *huongPhanXa, float mu ) {
   
   Vecto *huongAnhSang = &(matTroi->huongAnh);
   float doSang = huongAnhSang->x*huongPhanXa->x + huongAnhSang->y*huongPhanXa->y + huongAnhSang->z*huongPhanXa->z;
   
   // ---- hướng ánh sáng nên nghích chiếu với pháp tuyến
   doSang = -doSang;

   if( doSang < 0.0f )
      doSang = 0.0f;
   else
      doSang = powf( doSang, mu );

   Mau mauCaoQuang;
   mauCaoQuang.d = doSang*matTroi->mauAnh.d;
   mauCaoQuang.l = doSang*matTroi->mauAnh.l;
   mauCaoQuang.x = doSang*matTroi->mauAnh.x;
   mauCaoQuang.dd = doSang*matTroi->mauAnh.dd;
   
   return mauCaoQuang;
}


Mau toSuongMu( Vecto *huongTia, Mau *mauVat, Mau *mauTroi ) {

   Mau ketQua;
   // ---- chỉ tô màu vật thể nếu xa hơn 10.0f
   if( (mauVat->c > 10.0f) && (mauVat->c < kVO_CUC) ) {
      float tiSo = expf( (10.0f - mauVat->c)*0.0005f );
      float tiSoNguoc = 1.0f - tiSo;
      ketQua.d = mauVat->d*tiSo + tiSoNguoc*mauTroi->d;
      ketQua.l = mauVat->l*tiSo + tiSoNguoc*mauTroi->l;
      ketQua.x = mauVat->x*tiSo + tiSoNguoc*mauTroi->x;
      ketQua.dd = mauVat->dd*tiSo + tiSoNguoc*mauTroi->dd;
   }
   else {  // không có sương mù, chỉ tô màu vật thể
      ketQua.d = mauVat->d;
      ketQua.l = mauVat->l;
      ketQua.x = mauVat->x;
      ketQua.dd = mauVat->dd;
   }

   return ketQua;
}