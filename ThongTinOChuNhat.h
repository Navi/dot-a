#pragma once

/* Thông Tin Ô Chữ Nhật */
typedef struct {
   
   unsigned short beRong;  // bề rộng ô
   unsigned short beCao;   // bề cap ô

   unsigned short soLuongCot;   // số lượng cột
   unsigned short soLuongHang;  // số lượng hàng
   
   unsigned short x;   // số ô x của ô đang tính
   unsigned short y;   // số ô y của ô đang tính
   
   short cotBatDau;   // cột bắt đầu của ô đang tính
   short hangBatDau;  // hàng bắt đầu của ô đang tính
   short cotKetThuc;  // cột kết thúc của ô đang tính
   short hangKetThuc; // hàng kết thúc của ô đang tính

   unsigned long viTriTrongBangThanhPhan; // vị trí (byte) trong bảng thông tin chứa địa chỉ cho ô đang tính

   float *kenhDo;     // kênh đỏ
   float *kenhLuc;    // kênh lục
   float *kenhXanh;   // kênh xanh
   float *kenhDuc;    // kênh đục
   float *kenhXa;     // kênh cách xa

   unsigned char *thongTinGom;  // thông tin gồm
   unsigned char *thongTinLoc;  // thông tin lọc
   unsigned char *thongTinNen;  // thông tin nén
   
   unsigned char tinhTiep;   // tính tiếp ảnh bắt đầu (đã có ảnh chưa tính xong)

} ThongTinOChuNhat;

/* Đặt Thông Tin Ô Chữ Nhật */
void datThongTinOChuNhat( ThongTinOChuNhat *thongTinOChuNhat, unsigned short beRong, unsigned short beCao, unsigned short beRongAnh, unsigned short beCaoAnh );

/* Xóa Thông Tin Ô Chữ Nhật */
void xoaThongTinOChuNhat( ThongTinOChuNhat *thongTinOChuNhat );
