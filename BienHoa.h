#include "Vecto.h"
#include "Quaternion.h"
#include "../VatThe/VatThe.h"


#pragma mark Ma Trận
// kèm thêm ở đang sau như này: [vectơ] * [phóng to] * [xoay] * [dịch]
//                      nghịch: [vectơ] * [dịch]-1 *[xoay]-1 * [phóng to]-1
void datDonVi( float *maTran );  // đặt đơn vị (ma trận)
void datPhongTo( float *maTran, float phongToX, float phongToY, float phongToZ );  // đặt phóng to
void datDich( float *maTran, float x, float y, float z ); // đặt dịch
//void daoNghich4x4_datBiet( float *maTranDaoNguoc, float *maTranGoc );    // đảo nghịch 4x4 đặc biệt

#pragma mark maTran
void latMaTran4x4( float *maTran );
Vecto nhanVectoVoiMaTran3x3( Vecto *vecto, float *maTran );  // nhân vectơ với ma trận 3 x 3; không gồm dịch
Vecto nhanVectoVoiMaTran4x4( Vecto *vecto, float *maTran );  // nhân vectơ với ma trận 4 x 4;
void nhanMaTranVoiMaTran( float *maTranKetQua, float *maTran1, float *maTran2 );  // nhân ma trận với ma trận
void dinhHuongMaTranBangVectoNhin( float *maTran, Vecto *vectoNhin );


void datBienHoaChoVat( VatThe *vatThe, Vecto *phongTo, Quaternion *xoay, Vecto *dich );

void gomBienHoaChoVat( VatThe *vatThe );

Vecto viTriVatThe( VatThe *vatThe );

#pragma mark ---- Di Chuyển
Vecto xoayVectoQuanhTruc( Vecto *vecto, Vecto *trucXoay, float gocXoay );

Vecto viTriSin( Vecto *viTriDau, Vecto *huong, float bienDo, float soBucAnh, float soBucAnhChuKy, float *tocDo );

Vecto viTriSin2( Vecto *viTriDau, Vecto *huong, float bienDo, float soBucAnh, float soBucAnhChuKy, float *tocDo );

