// Mau.h 
// 2558-12-16

#pragma once

/* Màu */
typedef struct {
   float d;  // đỏ
   float l;  // lục
   float x;  // xanh
   float dd; // độ đục
   float c;  // cách xa
   float p;  // phản xạ
} Mau;