#include "Mau.h"
#include "Toan/Vecto.h"
#include "Toan/Tia.h"
#include "VatThe/VatThe.h"
#include "VatThe/MatTroi.h"

/* Tô màu vật */
Mau toMauVat( VatThe *vatThe, Vecto *diemTrungTDVT, Vecto phapTuyen, Vecto huongTia );

/* tính màu trời */
Mau tinhMauTroi( Vecto *huongTia );

/* tính màu tán xạ */
Mau tinhMauTanXa( Mau *mauMatTroi, Mau *mauVat, Vecto *huongNguonAnhSang, Vecto *phapTuyen );

/* Tính tia phản xạ */
Tia tinhTiaPhanXa( Vecto phapTuyen, Vecto diemTrung, Vecto huongTrung );

/* Tính tia khúc xạ */
Tia tinhTiaKhucXa( Vecto phapTuyen, Vecto diemTrung, Vecto huongTrung, float chietSuat );

/* tính cao quang */
Mau tinhCaoQuang( MatTroi *matTroi, Vecto *huongPhanXa, float mu );

/* Tô sương mù */
Mau toSuongMu( Vecto *huongTia, Mau *mauVat, Mau *mauTroi );