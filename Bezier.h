#include "Vecto.h"

/* Bezier */
typedef struct {
   Vecto diemQuanTri[4];  // danh sách điểm quản trị
//   unsigned short soLuongDiem;  // số lượng điểm
} Bezier;


/* Tính vị trí Bezier */
Vecto tinhViTriBezier3C( Bezier *bezier, float thamSo );

/* Tính vận tốc Beier */
Vecto tinhVanTocBezier3C( Bezier *bezier, float thamSo );

/* Tính gia tốc Beier */
Vecto tinhGiaTocBezier3C( Bezier *bezier, float thamSo );
