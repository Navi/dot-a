#pragma once
#include "Anh.h"
#include "ThongTinOChuNhat.h"

/* có kênh đỏ, lục, xanh */
void luuAnhEXR_ZIP( Anh *anh, unsigned char kieuDuLieu, unsigned short thoiGianKetXuat );

/* có thêm kênh cách xa */
void luuAnhEXR_ZIP_Z( Anh *anh, unsigned char kieuDuLieu, unsigned short thoiGianKetXuat );

// ---- hàm cho trường hợp máy không ổ định, dễ bị mất điện, v.v. khi tính ảng to
void chuanBiLuuAnhEXR_O_ZIP( Anh *anh, ThongTinOChuNhat *thongTinOChuNhat, unsigned char kieuDuLieu, unsigned short thoiGianKetXuat );

/* Lưu Ô Chữ Nhật ZIP */
void luuOChuNhatZIP( char *tenTep, ThongTinOChuNhat *thongTinOChuNhat, unsigned char kieuDuLieu );
