#include "ThongTinOChuNhat.h"
#include <stdio.h>
#include <stdlib.h>

#pragma mark ---- Chuẩn Bị
void datThongTinOChuNhat( ThongTinOChuNhat *thongTinOChuNhat, unsigned short beRong, unsigned short beCao, unsigned short beRongAnh, unsigned short beCaoAnh ) {

   // ---- bề rộng ô
   thongTinOChuNhat->beRong = beRong;
   thongTinOChuNhat->beCao = beCao;
   
   // ---- tíng số hàng và cột ô
   thongTinOChuNhat->soLuongCot = beRongAnh / beRong;
   thongTinOChuNhat->soLuongHang = beCaoAnh / beCao;
   
   // ---- có một cột ô hẹp không?
   if( beRongAnh % beRong )
      thongTinOChuNhat->soLuongCot++;
   
   // ---- có một hàng ô lùn không?
   if( beCaoAnh % beCao )
      thongTinOChuNhat->soLuongHang++;

   // ---- đặt số ô bắt đầu
   thongTinOChuNhat->x = 0;
   thongTinOChuNhat->y = 0;
   
   // ---- các kênh
   unsigned int soLuongDiemAnh = beRong*beCao;
   thongTinOChuNhat->kenhDo = malloc( sizeof( float )*soLuongDiemAnh );
   thongTinOChuNhat->kenhLuc = malloc( sizeof( float )*soLuongDiemAnh );
   thongTinOChuNhat->kenhXanh = malloc( sizeof( float )*soLuongDiemAnh );
   thongTinOChuNhat->kenhDuc = malloc( sizeof( float )*soLuongDiemAnh );
   thongTinOChuNhat->kenhXa = malloc( sizeof( float )*soLuongDiemAnh );

   // ---- các đệm để chứa, lọc, nén dỡ liệu để lưu
   thongTinOChuNhat->thongTinGom = malloc( sizeof( float )*soLuongDiemAnh << 3 );
   thongTinOChuNhat->thongTinLoc = malloc( sizeof( float )*soLuongDiemAnh << 3 );
   thongTinOChuNhat->thongTinNen = malloc( sizeof( float )*soLuongDiemAnh << 4 );
}


void xoaThongTinOChuNhat( ThongTinOChuNhat *thongTinOChuNhat ) {

   // ---- bỏ các kênh
   free( thongTinOChuNhat->kenhDo );
   free( thongTinOChuNhat->kenhLuc );
   free( thongTinOChuNhat->kenhXanh );
   free( thongTinOChuNhat->kenhDuc );
//   free( thongTinOChuNhat->kenhXa );

   // ---- bỏ các đệm
   free( thongTinOChuNhat->thongTinGom );
   free( thongTinOChuNhat->thongTinLoc );
   free( thongTinOChuNhat->thongTinNen );
}
