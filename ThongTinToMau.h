#pragma once
#include "VatThe/VatThe.h"
#include "Toan/Vecto.h"

/* Thông Tin Tô Màu */
typedef struct {
   VatThe *vatThe;         // vật thể
   Vecto phapTuyenTDVT;    // pháp tuyến (tọa đồ vật thể)
   Vecto diemTrungTDVT;    // điểm trúng (tọa đồ vật thể)
   float cachXa;       // cách xa
} ThongTinToMau;