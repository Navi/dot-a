#pragma once
#include "Toan/Vecto.h"

/* Ảnh */
typedef struct {
   unsigned short beRong;   // bề rộng
   unsigned short beCao;    // bề cao
   float coKichDiemAnh;     // cỡ kích điểm ảnh (điểm anh/đơn vị thế giới)
   float *kenhDo;      // kênh đỏ
   float *kenhLuc;     // kênh lục
   float *kenhXanh;    // kênh xanh
   float *kenhDuc;     // kênh đục
   float *kenhXa;    // kênh cách xa

   char ten[256];     // tên ảnh
} Anh;

/* Tạo ảnh với cỡ kích */
Anh taoAnhVoiCoKich( unsigned short beRong, unsigned short beCao, float coKichDiemAnh );

/* Đặt tên ảnh */
void datTenAnh( Anh *anh, char *tenMoi );

/* Xóa ảnh */
void xoaAnh( Anh *anh );