// Nhân Vặt
// 2016.11.15
#include "NhanVat.h"
#include "Mau.h"
#include "Toan/Quaternion.h"
#include "Toan/BienHoa.h"


unsigned short datVaiChanh( VatThe *danhSachVat, Vecto viTri ) {
   
   Vecto truc;
   truc.x = 1.0f;    truc.y = 1.0f;    truc.z = 0.0f;
   Quaternion quaternion = datQuaternionTuVectoVaGocQuay( &truc, 1.570795f );
   
   Vecto phongTo;
   phongTo.x = 1.0f;     phongTo.y = 1.0f;     phongTo.z = 1.0f;
   
   danhSachVat[0].hinhDang.hinhCau = datHinhCau( 0.7f, &(danhSachVat[0].baoBiVT) );
   danhSachVat[0].loai = kLOAI_HINH_DANG__HINH_CAU;
   danhSachVat[0].chietSuat = 1.0f;
   datBienHoaChoVat( &(danhSachVat[0]), &phongTo, &quaternion, &viTri );
   //   danhSachVat[0].hoaTiet.hoaTietKhong = kHOA_TIET__TRAI_BANH;
   danhSachVat[0].soHoaTiet = kHOA_TIET__TRAI_BANH;
   
   return 1;
}


unsigned short datKeThuSaoGaiDo( VatThe *danhSachVat, Vecto viTri ) {
   Quaternion quaternion;
   quaternion.w = 1.0f;    quaternion.x = 0.0f;     quaternion.y = 0.0f;     quaternion.z = 0.0f;
   
   Vecto phongTo;
   phongTo.x = 1.0f;     phongTo.y = 1.0f;     phongTo.z = 1.0f;
   
   Mau mau;

   // ---- đấy - lớp 0
   mau.d = 0.8f;      mau.l = 0.0f;      mau.x = 0.0f;      mau.dd = 1.0f;   mau.p = 0.2f;
   danhSachVat[0].hinhDang.saoGai = datSaoGai( &(danhSachVat[0].baoBiVT), 1.0f );
   danhSachVat[0].loai = kLOAI_HINH_DANG__SAO_GAI;
   danhSachVat[0].chietSuat = 1.0f;
   datBienHoaChoVat( &(danhSachVat[0]), &phongTo, &quaternion, &viTri );
   danhSachVat[0].hoaTiet.hoaTietKhong = datHoaTietKhong( &mau );
   danhSachVat[0].soHoaTiet = kHOA_TIET__KHONG;
   
   danhSachVat[0].thaiTrang = 0;
   
   return 1;
}

unsigned short datKeThuSaoGaiXanh( VatThe *danhSachVat, Vecto viTri ) {
   Quaternion quaternion;
   quaternion.w = 1.0f;    quaternion.x = 0.0f;     quaternion.y = 0.0f;     quaternion.z = 0.0f;
   
   Vecto phongTo;
   phongTo.x = 1.0f;     phongTo.y = 1.0f;     phongTo.z = 1.0f;
   
   Mau mau;
   
   // ---- đấy - lớp 0
   mau.d = 0.0f;      mau.l = 0.0f;      mau.x = 0.8f;      mau.dd = 1.0f;   mau.p = 0.2f;
   danhSachVat[0].hinhDang.saoGai = datSaoGai( &(danhSachVat[0].baoBiVT), 3.0f );
   danhSachVat[0].loai = kLOAI_HINH_DANG__SAO_GAI;
   danhSachVat[0].chietSuat = 1.0f;
   datBienHoaChoVat( &(danhSachVat[0]), &phongTo, &quaternion, &viTri );
   danhSachVat[0].hoaTiet.hoaTietKhong = datHoaTietKhong( &mau );
   danhSachVat[0].soHoaTiet = kHOA_TIET__KHONG;
   
   danhSachVat[0].thaiTrang = 0;
   
   return 1;
}

unsigned short datTraiBanhBiGiet( VatThe *danhSachVat ) {
   Quaternion quaternion;
   quaternion.w = 1.0f;    quaternion.x = 0.0f;     quaternion.y = 0.0f;     quaternion.z = 0.0f;
   
   Vecto phongTo;
   phongTo.x = 1.0f;     phongTo.y = 1.0f;     phongTo.z = 1.0f;
   
   Vecto viTri;
   
   float banKinh = 2.2f;
   
   Mau mauNen;
   mauNen.d = 1.0f;      mauNen.l = 1.0f;      mauNen.x = 1.0f;      mauNen.dd = 1.0f;   mauNen.p = 0.2f;
   Mau mauOc0;
   mauOc0.d = 1.0f;      mauOc0.l = 0.3f;      mauOc0.x = 0.3f;      mauOc0.dd = 1.0f;   mauOc0.p = 0.2f;
   Mau mauOc1;
   mauOc1.d = 0.5f;      mauOc1.l = 0.5f;      mauOc1.x = 0.5f;      mauOc1.dd = 1.0f;   mauOc1.p = 0.2f;
   Mau mauOc2;
   mauOc2.d = 0.5f;      mauOc2.l = 1.0f;      mauOc2.x = 0.5f;      mauOc2.dd = 1.0f;   mauOc2.p = 0.2f;
   
   // ---- thật vị trí không quan trọn
   viTri.x = -3.0f;      viTri.y = 10.1 + banKinh;        viTri.z = -523.0f;
   danhSachVat[0].hinhDang.hinhCau = datHinhCau( banKinh, &(danhSachVat[0].baoBiVT) );
   danhSachVat[0].loai = kLOAI_HINH_DANG__HINH_CAU;
   danhSachVat[0].chietSuat = 1.0f;
   datBienHoaChoVat( &(danhSachVat[0]), &phongTo, &quaternion, &viTri );
   danhSachVat[0].hoaTiet.hoaTietQuanXoay = datHoaTietQuanXoay( &mauNen, &mauOc0, &mauOc1, &mauOc2, 0.05f, 0.08f, 0.15f, 0.0f, 0.165f, 1 );
   danhSachVat[0].soHoaTiet = kHOA_TIET__QUAN_XOAY;
   
   danhSachVat[0].thaiTrang = 0;
   
   return 1;
}
