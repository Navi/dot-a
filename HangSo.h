// HằngSố.h
// 2558-12-16

#pragma once

#define kSAI  0
#define kDUNG 1

#define kKIEU_HALF 1
#define kKIEU_FLOAT 2

#define kTO_CHUC_HANG 0        // ảnh hàng
#define kTO_CHUC_O_CHU_NHAT 1  // ảnh ô chữ nhật

//#define kTINH_TIEP_THEO 1   // tính từ đầu (tập tin mới)

#define kKICH_THUOC_O_CHU_NHTA_MAC_DINH 128  // kich thước ô chữ nhật mặc định

#define kKHONG_BIH 0

#define kHAP_DAN -0.064f   // hấp dẫn thế giới

#define kVO_CUC 1.0e9f    // gía trị cho vô cực, cũng xài cho biết tia nào không trúng vật thể

#define kSO_LUONG_VAT_THE_TOI_DA 16384    // số lượng tố đa có yhể chứa

#define kSO_NGUYEN_TOI_DA  2147483647.0f // <--- cho 64 bit? // 32767.0f // <--- cho 32 bit?    // cho đơn vị hóa số ngẫu nhiên 

#define kSO_LUONG__GIAO_DIEM_TOI_DA   114688


/* Trục/Hướng */
// ---- Xài 3 bit cao 
#define kTRUC_X 0x00   // 0 0 00
#define kTRUC_Y 0x01   // 0 0 01
#define kTRUC_Z 0x02   // 0 0 10
//#define kHOP    0x04   // 0 1 00

// ---- Cho biết trúng mặt trên hay dưới (hình trụ, v.v.)
#define kTRUNG_DUOI 1
#define kTRUNG_TREN 2


#define kSAO_GAI__BAN_KINH     1.5f

#define kSO_LUONG__VAT_THE_BONG_TOI_DA 16
