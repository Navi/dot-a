#include "Anh.h"
#include "Toan/Vecto.h"
#include <stdlib.h>
#include <math.h>

#pragma mark ---- Ảnh
Anh taoAnhVoiCoKich( unsigned short beRong, unsigned short beCao, float coKichDiemAnh ) {
 
   Anh anh;
   anh.beRong = beRong;
   anh.beCao = beCao;
   anh.coKichDiemAnh = coKichDiemAnh;

   // ---- dành trí nhớ
   anh.kenhDo = malloc( sizeof( float)*beRong*beCao );
   anh.kenhLuc = malloc( sizeof( float)*beRong*beCao );
   anh.kenhXanh = malloc( sizeof( float)*beRong*beCao );
   anh.kenhDuc = malloc( sizeof( float)*beRong*beCao );
   anh.kenhXa = malloc( sizeof( float)*beRong*beCao );

   return anh;
}


void datTenAnh( Anh *anh, char *tenMoi ) {
   
   unsigned short chiSoKyTu = 0;
   
   while( (chiSoKyTu < 255) && (tenMoi != 0x00) ) {
      anh->ten[chiSoKyTu] = tenMoi[chiSoKyTu];
      chiSoKyTu++;
   }
}


void xoaAnh( Anh *anh ) {
   
   anh->beRong = 0;
   anh->beCao = 0;
   
   free( anh->kenhDo );
   free( anh->kenhLuc );
   free( anh->kenhXanh );
   free( anh->kenhDuc );
//   free( anh->kenhXa );
}
