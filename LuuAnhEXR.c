
#include "LuuAnhEXR.h"
#include "HangSo.h"
#include <stdio.h>
#include <stdlib.h>
#include <zlib.h>

#pragma mark ---- Lưư Ảnh
void luuThongTinPhienBan( FILE *tep, unsigned char coO );
void luuThongTinKenh_EXR( FILE *tep, unsigned char *danhSachKenh, unsigned char soLuongKenh, unsigned char kieuDuLieu );

void luuThongTinCuaSoDuLieu( FILE *tep, unsigned int beRong, unsigned int beCao );
void luuThongTinCuaSoChieu( FILE *tep, unsigned int beRong, unsigned int beCao );
void luuThongTinNenZIP( FILE *tep );
void luuThongTinThuTuHang( FILE *tep, unsigned char thuTu );
void luuThongTinDiemAnh( FILE *tep );
void luuThongTinManChieu( FILE *tep );
void luuThongTinOChuNhat( FILE *tep, unsigned int beRongO, unsigned int beCaoO, unsigned char soLuongTang );
void luuThoiGianKetXuat( FILE *tep, unsigned short thoiGianKetXuat );
void luuBangDuLieuAnh( FILE *tep, unsigned short soLuongThanhPhan );
void chepDuLieuKenhFloat( unsigned char *dem, const float *kenh, unsigned short beRong );
void chepDuLieuKenhHalf( unsigned char *dem, const float *kenh, unsigned short beRong );
unsigned short doiFloatSangHalf( float soFloat );
void locDuLieuTrongDem( unsigned char *dem, unsigned int beDai, unsigned char *demLoc );
unsigned int nenZIP( unsigned char *dem, int beDaiDem, unsigned char *demNen, int beDaiDemNen );

/* Lưu Ảnh EXR nén ZIP */
void luuAnhEXR_ZIP( Anh *anh, unsigned char kieuDuLieu, unsigned short thoiGianKetXuat ) {
   
   FILE *tep = fopen( anh->ten, "wb" );
//   luuThongTinKenhEXR( unsigned short beRong, unsigned short beCao );
   // ---- mã số EXR
   fputc( 0x76, tep );
   fputc( 0x2f, tep );
   fputc( 0x31, tep );
   fputc( 0x01, tep );

   // ---- phiên bản 2 (chỉ phiên bản 2 được phát hành)
   luuThongTinPhienBan( tep, 0x00 ); // cờ ô 0x00 = ảnh hàng
   
   unsigned short beRong = anh->beRong;
   unsigned short beCao = anh->beCao;

   // ---- thông cho các kênh
   unsigned char danhSachKenh[4] = {'B', 'G', 'R', 0};
   luuThongTinKenh_EXR( tep, danhSachKenh, 3, kieuDuLieu );
   
   // ---- nén
   luuThongTinNenZIP( tep );

   // ---- cửa sổ dữ liệu
   luuThongTinCuaSoDuLieu( tep, beRong, beCao );

   // ---- cửa sổ dữ liệu
   luuThongTinCuaSoChieu( tep, beRong, beCao );
   
   // ---- thứ tự hàng (từ nhỏ đến lớn)
   luuThongTinThuTuHang(tep, 0x00 );
   
   // ---- tỉ số cạnh điểm ảnh
   luuThongTinDiemAnh( tep );
   // ---- thời gian kết xuất
   luuThoiGianKetXuat( tep, thoiGianKetXuat );
   // ---- thông tin màn
   luuThongTinManChieu( tep );

   // ---- kết thúc phần đầu
   fputc( 0x00, tep );
   
   // ==== bảng cho thành phần dữ liệu ảnh
   // ---- giữ địa chỉ đầu bảng thành phần
   unsigned long long diaChiDauBangThanhPhan = ftell( tep );
   unsigned short soLuongThanhPhan = beCao >> 4;
   // ---- nếu có phần dư, cần thêm một thành phần
   if( beCao & 0xf )
      soLuongThanhPhan++;

   // ---- lưu bàng rỗng
   luuBangDuLieuAnh( tep, soLuongThanhPhan );

   unsigned long long *bangThanhPhan = malloc( soLuongThanhPhan << 3 );  // sốLượngThanhPhần * 8

   // ---- bề dài dệm
   unsigned int beDaiDem = (beRong << kieuDuLieu)*3 << 4; // nhân 3 cho 3 kênh, 16 hàng
   // ---- tạo đệm để lọc dữ liệu
   unsigned char *dem = malloc( beDaiDem );
   unsigned char *demLoc = malloc( beDaiDem );
   unsigned char *demNenZIP = malloc( beDaiDem << 1);  // nhân 2 cho an toàn
   unsigned short soThanhPhan = 0;
   
   // ---- lưu dữ liệu cho thành phần ảnh
   unsigned short soHang = 0;
   while( soHang < beCao ) {
      
      // ---- tính số lượng hàng còn
      unsigned short soLuongHangCon = beCao - soHang;
      if( soLuongHangCon > 16 )
         soLuongHangCon = 16;
      else
         beDaiDem = (beDaiDem >> 4)*soLuongHangCon;   // bề dài đệm cho thành phần dư (không đủ 16 hàng)
      
      bangThanhPhan[soThanhPhan] = ftell( tep );
      soThanhPhan++;

      // ---- luư số hàng
      fputc( soHang & 0xff, tep );
      fputc( (soHang >> 8), tep );
      fputc( (soHang >> 16), tep );
      fputc( (soHang >> 24), tep );
      
      // ---- dữ liệu kênh
      unsigned int diaChiKenh = beRong*soHang;
      unsigned int diaChiDem = 0;
      
      // ---- gồm dữ liệu từ các hàng
      unsigned char soHangTrongThanhPhan = 0;
      
      while( soHangTrongThanhPhan < soLuongHangCon ) {
         
         // ---- tùy kiểu dữ liệu trong ảnh
         if( kieuDuLieu == kKIEU_FLOAT ) {
            // ---- chép kênh xanh
            chepDuLieuKenhFloat( &(dem[diaChiDem]), &(anh->kenhXanh[diaChiKenh]), beRong );
            // ---- chép kênh lục
            diaChiDem += beRong << kieuDuLieu;
            chepDuLieuKenhFloat( &(dem[diaChiDem]), &(anh->kenhLuc[diaChiKenh]), beRong );
            // ---- chép kênh đỏ
            diaChiDem += beRong << kieuDuLieu;
            chepDuLieuKenhFloat( &(dem[diaChiDem]), &(anh->kenhDo[diaChiKenh]), beRong );
            // ---- tiếp theo
            diaChiDem += beRong << kieuDuLieu;
         }
         else {  // kKIEU_HALF
            // ---- chép kênh xanh
            chepDuLieuKenhHalf( &(dem[diaChiDem]), &(anh->kenhXanh[diaChiKenh]), beRong );
            // ---- chép kênh lục
            diaChiDem += beRong << kieuDuLieu;
            chepDuLieuKenhHalf( &(dem[diaChiDem]), &(anh->kenhLuc[diaChiKenh]), beRong );
            // ---- chép kênh đỏ
            diaChiDem += beRong << kieuDuLieu;
            chepDuLieuKenhHalf( &(dem[diaChiDem]), &(anh->kenhDo[diaChiKenh]), beRong );
            // ---- tiếp theo
            diaChiDem += beRong << kieuDuLieu;
         }
         // ---- hàng tiếp theo
         diaChiKenh += beRong;

         soHangTrongThanhPhan++;
      }

      locDuLieuTrongDem( dem, beDaiDem, demLoc);
      unsigned int beDaiDuLieuNen = nenZIP( demLoc, beDaiDem, demNenZIP, beDaiDem << 1 );
     
      fputc( beDaiDuLieuNen & 0xff, tep );
      fputc( (beDaiDuLieuNen >> 8), tep );
      fputc( (beDaiDuLieuNen >> 16), tep );
      fputc( (beDaiDuLieuNen >> 24), tep );
      
      // ---- lưu dữ liệu nén
      unsigned int diaChi = 0;
      while( diaChi < beDaiDuLieuNen ) {
         fputc( demNenZIP[diaChi], tep );
         diaChi++;
      }

      soHang += 16;
   }
   
   // ---- lưu bảng thành phân
   fseek( tep, diaChiDauBangThanhPhan, SEEK_SET );
   soHang = 0;

   soThanhPhan = 0;
   while( soThanhPhan < soLuongThanhPhan ) {
      unsigned long long diaChiThanhPhan = bangThanhPhan[soThanhPhan];
      fputc( diaChiThanhPhan & 0xff, tep );
      fputc( (diaChiThanhPhan >> 8), tep );
      fputc( (diaChiThanhPhan >> 16), tep );
      fputc( (diaChiThanhPhan >> 24), tep );
      fputc( (diaChiThanhPhan >> 32), tep );
      fputc( (diaChiThanhPhan >> 40), tep );
      fputc( (diaChiThanhPhan >> 48), tep );
      fputc( (diaChiThanhPhan >> 56), tep );
      soThanhPhan++;
   }
 
   // ---- thả trí nhớ
   free( dem );
   free( demLoc );
   free( demNenZIP );
   // ---- đóng tệp
   fclose( tep );
}

/* Lưu Ảnh EXR nén ZIP - có kênh cách xa */
void luuAnhEXR_ZIP_Z( Anh *anh, unsigned char kieuDuLieu, unsigned short thoiGianKetXuat ) {
   
   FILE *tep = fopen( anh->ten, "wb" );
   //   luuThongTinKenhEXR( unsigned short beRong, unsigned short beCao );
   // ---- mã số EXR
   fputc( 0x76, tep );
   fputc( 0x2f, tep );
   fputc( 0x31, tep );
   fputc( 0x01, tep );
   
   // ---- phiên bản 2 (chỉ phiên bản 2 được phát hành)
   luuThongTinPhienBan( tep, 0x00 ); // cờ ô 0x00 = ảnh hàng
   
   unsigned short beRong = anh->beRong;
   unsigned short beCao = anh->beCao;
   
   // ---- thông cho các kênh
   unsigned char danhSachKenh[5] = {'B', 'G', 'R', 'Z', 0};
   luuThongTinKenh_EXR( tep, danhSachKenh, 4, kieuDuLieu );
   
   // ---- nén
   luuThongTinNenZIP( tep );
   
   // ---- cửa sổ dữ liệu
   luuThongTinCuaSoDuLieu( tep, beRong, beCao );
   
   // ---- cửa sổ dữ liệu
   luuThongTinCuaSoChieu( tep, beRong, beCao );
   
   // ---- thứ tự hàng (từ nhỏ đến lớn)
   luuThongTinThuTuHang(tep, 0x00 );
   
   // ---- tỉ số cạnh điểm ảnh
   luuThongTinDiemAnh( tep );
   // ---- thời gian kết xuất
   luuThoiGianKetXuat( tep, thoiGianKetXuat );
   // ---- thông tin màn
   luuThongTinManChieu( tep );
   
   // ---- kết thúc phần đầu
   fputc( 0x00, tep );
   
   // ==== bảng cho thành phần dữ liệu ảnh
   // ---- giữ địa chỉ đầu bảng thành phần
   unsigned long long diaChiDauBangThanhPhan = ftell( tep );
   unsigned short soLuongThanhPhan = beCao >> 4;
   // ---- nếu có phần dư, cần thêm một thành phần
   if( beCao & 0xf )
      soLuongThanhPhan++;
   
   // ---- lưu bàng rỗng
   luuBangDuLieuAnh( tep, soLuongThanhPhan );
   
   unsigned long long *bangThanhPhan = malloc( soLuongThanhPhan << 3 );  // sốLượngThanhPhần * 8
   
   // ---- bề dài dệm
   //      nhân 3 cho 3 kênh, 16 hàng
   //     2 * 3 + 4, kênh cách xa lần nào là kiểu float
   unsigned int beDaiDuLieuDiemAnh = (1 << kieuDuLieu)*3 + 4;
   unsigned int beDaiDem = beRong*beDaiDuLieuDiemAnh << 4;   // << 4 = 16
   // ---- tạo đệm để lọc dữ liệu
   unsigned char *dem = malloc( beDaiDem );
   unsigned char *demLoc = malloc( beDaiDem );
   unsigned char *demNenZIP = malloc( beDaiDem << 1);  // nhân 2 cho an toàn
   unsigned short soThanhPhan = 0;
   
   // ---- lưu dữ liệu cho thành phần ảnh
   unsigned short soHang = 0;
   while( soHang < beCao ) {
      
      // ---- tính số lượng hàng còn
      unsigned short soLuongHangCon = beCao - soHang;
      if( soLuongHangCon > 16 )
         soLuongHangCon = 16;
      else
         beDaiDem = (beDaiDem >> 4)*soLuongHangCon;   // bề dài đệm cho thành phần dư (không đủ 16 hàng)
      
      bangThanhPhan[soThanhPhan] = ftell( tep );
      soThanhPhan++;
      
      // ---- luư số hàng
      fputc( soHang & 0xff, tep );
      fputc( (soHang >> 8), tep );
      fputc( (soHang >> 16), tep );
      fputc( (soHang >> 24), tep );
      
      // ---- dữ liệu kênh
      unsigned int diaChiKenh = beRong*soHang;
      unsigned int diaChiDem = 0;
      
      // ---- gồm dữ liệu từ các hàng
      unsigned char soHangTrongThanhPhan = 0;
      
      while( soHangTrongThanhPhan < soLuongHangCon ) {
         
         // ---- tùy kiểu dữ liệu trong ảnh
         if( kieuDuLieu == kKIEU_FLOAT ) {
            // ---- chép kênh xanh
            chepDuLieuKenhFloat( &(dem[diaChiDem]), &(anh->kenhXanh[diaChiKenh]), beRong );
            // ---- chép kênh lục
            diaChiDem += beRong << kieuDuLieu;
            chepDuLieuKenhFloat( &(dem[diaChiDem]), &(anh->kenhLuc[diaChiKenh]), beRong );
            // ---- chép kênh đỏ
            diaChiDem += beRong << kieuDuLieu;
            chepDuLieuKenhFloat( &(dem[diaChiDem]), &(anh->kenhDo[diaChiKenh]), beRong );
            // ---- tiếp theo
            diaChiDem += beRong << kieuDuLieu;
         }
         else {  // kKIEU_HALF
            // ---- chép kênh xanh
            chepDuLieuKenhHalf( &(dem[diaChiDem]), &(anh->kenhXanh[diaChiKenh]), beRong );
            // ---- chép kênh lục
            diaChiDem += beRong << kieuDuLieu;
            chepDuLieuKenhHalf( &(dem[diaChiDem]), &(anh->kenhLuc[diaChiKenh]), beRong );
            // ---- chép kênh đỏ
            diaChiDem += beRong << kieuDuLieu;
            chepDuLieuKenhHalf( &(dem[diaChiDem]), &(anh->kenhDo[diaChiKenh]), beRong );
            // ---- tiếp theo
            diaChiDem += beRong << kieuDuLieu;
         }
         // ---- kênh các xa
         chepDuLieuKenhFloat( &(dem[diaChiDem]), &(anh->kenhXa[diaChiKenh]), beRong );
         diaChiDem += beRong << kKIEU_FLOAT;
         
         // ---- hàng tiếp theo
         diaChiKenh += beRong;
         
         soHangTrongThanhPhan++;
      }
      
      locDuLieuTrongDem( dem, beDaiDem, demLoc);
      unsigned int beDaiDuLieuNen = nenZIP( demLoc, beDaiDem, demNenZIP, beDaiDem << 1 );
      
      fputc( beDaiDuLieuNen & 0xff, tep );
      fputc( (beDaiDuLieuNen >> 8), tep );
      fputc( (beDaiDuLieuNen >> 16), tep );
      fputc( (beDaiDuLieuNen >> 24), tep );
      
      // ---- lưu dữ liệu nén
      unsigned int diaChi = 0;
      while( diaChi < beDaiDuLieuNen ) {
         fputc( demNenZIP[diaChi], tep );
         diaChi++;
      }
      
      soHang += 16;
   }
   
   // ---- lưu bảng thành phân
   fseek( tep, diaChiDauBangThanhPhan, SEEK_SET );
   soHang = 0;
   
   soThanhPhan = 0;
   while( soThanhPhan < soLuongThanhPhan ) {
      unsigned long long diaChiThanhPhan = bangThanhPhan[soThanhPhan];
      fputc( diaChiThanhPhan & 0xff, tep );
      fputc( (diaChiThanhPhan >> 8), tep );
      fputc( (diaChiThanhPhan >> 16), tep );
      fputc( (diaChiThanhPhan >> 24), tep );
      fputc( (diaChiThanhPhan >> 32), tep );
      fputc( (diaChiThanhPhan >> 40), tep );
      fputc( (diaChiThanhPhan >> 48), tep );
      fputc( (diaChiThanhPhan >> 56), tep );
      soThanhPhan++;
   }
   
   // ---- thả trí nhớ
   free( dem );
   free( demLoc );
   free( demNenZIP );
   // ---- đóng tệp
   fclose( tep );
}



void chuanBiLuuAnhEXR_O_ZIP( Anh *anh, ThongTinOChuNhat *thongTinOChuNhat, unsigned char kieuDuLieu, unsigned short thoiGianKetXuat ) {
   
   FILE *tep = fopen( anh->ten, "wb" );
   //   luuThongTinKenhEXR( unsigned short beRong, unsigned short beCao );
   // ---- mã số EXR
   fputc( 0x76, tep );
   fputc( 0x2f, tep );
   fputc( 0x31, tep );
   fputc( 0x01, tep );
   
   // ---- phiên bản 2 (chỉ phiên bản 2 được phát hành)
//   unsigned int phienBan = 0x02;
   luuThongTinPhienBan( tep, 0x02 ); // cờ ô 0x02 = ảnh ô
   
   unsigned short beRong = anh->beRong;
   unsigned short beCao = anh->beCao;
   
   // ---- thông cho các kênh
   unsigned char danhSachKenh[4] = {'B', 'G', 'R', 0};
   luuThongTinKenh_EXR( tep, danhSachKenh, 3, kieuDuLieu );
   
   // ---- nén
   luuThongTinNenZIP( tep );
   
   // ---- cửa sổ dữ liệu
   luuThongTinCuaSoDuLieu( tep, beRong, beCao );
   
   // ---- cửa sổ dữ liệu
   luuThongTinCuaSoChieu( tep, beRong, beCao );
   
   // ---- thứ tự hàng (từ nhỏ đến lớn)
   luuThongTinThuTuHang( tep, 0x00 );
   
   // ---- tỉ số cạnh điểm ảnh
   luuThongTinDiemAnh( tep );
   
   // ---- lưu thời gian
   luuThoiGianKetXuat( tep, thoiGianKetXuat );
   
   // ---- trung tâm cửa sổ màn
   luuThongTinManChieu( tep );
   
   // ---- thông tin về hình dạng các ô
   luuThongTinOChuNhat( tep, thongTinOChuNhat->beRong, thongTinOChuNhat->beCao, 0 ); // thời này chỉ hỗ trợ một tầng

   // ---- kết thúc phần đầu
   fputc( 0x00, tep );
   
   // ==== bảng cho thành phần dữ liệu ảnh
   // ---- giữ địa chỉ đầu bảng thành phần
   unsigned short soLuongThanhPhan = thongTinOChuNhat->soLuongCot*thongTinOChuNhat->soLuongHang;
   unsigned long long diaChiDauBangThanhPhan = ftell( tep );

   thongTinOChuNhat->viTriTrongBangThanhPhan = diaChiDauBangThanhPhan;

   // ---- lưu bàng rỗng
   luuBangDuLieuAnh( tep, soLuongThanhPhan );
   
   // ---- đóng tệp
   fclose( tep );
}


#pragma mark ---- THÔNG TIN KÊNH
void luuThongTinPhienBan( FILE *tep, unsigned char coO ) {
   
   // ---- phiên bản 2 (chỉ phiên bản 2 được phát hành)
   //   unsigned int phienBan = 0x02;
   fputc( 0x02, tep );
   fputc( coO , tep );  // cờ ô
   fputc( 0x00, tep );
   fputc( 0x00, tep );
}

void luuThongTinKenh_EXR( FILE *tep, unsigned char *danhSachKenh, unsigned char soLuongKenh, unsigned char kieuDuLieu ) {

   fprintf( tep, "channels" );
   fputc( 0x00, tep );
   fprintf( tep, "chlist" );
   fputc( 0x00, tep );
   unsigned char beDaiDuLieuKenh = soLuongKenh*18 + 1;
   fputc( beDaiDuLieuKenh, tep );   // bề dài cho n kênh, tên các kênh dài một chữ cái ASCII
   fputc( 0x00, tep );
   fputc( 0x00, tep );
   fputc( 0x00, tep );

   // ---- thông tin cho các kênh
   unsigned char soKenh = 0;
   while( soKenh < soLuongKenh ) {
      fputc( danhSachKenh[soKenh], tep );
      fputc( 0x00, tep );
      
      if( danhSachKenh[soKenh] == 'Z' )
         fputc( kKIEU_FLOAT, tep );
      else
         fputc( kieuDuLieu, tep );  // kiểu dữ liệu 0x02 nghỉa là float, 0x01 là half

      fputc( 0x00, tep );
      fputc( 0x00, tep );
      fputc( 0x00, tep );
      
      fputc( 0x00, tep );   // chỉ xài cho phương pháp nén B44, ở đây không xài
      fputc( 0x00, tep );
      fputc( 0x00, tep );
      fputc( 0x00, tep );
      
      fputc( 0x01, tep );  // nhịp x
      fputc( 0x00, tep );
      fputc( 0x00, tep );
      fputc( 0x00, tep );
      
      fputc( 0x01, tep );  // nhịp y
      fputc( 0x00, tep );
      fputc( 0x00, tep );
      fputc( 0x00, tep );

      soKenh++;
   }
   
   // ---- kết thúc danh sách kênh
   fputc( 0x00, tep );
}

void luuThongTinNenZIP( FILE *tep ) {
   // ----
   fprintf( tep, "compression" );
   fputc( 0x00, tep );
   fprintf( tep, "compression" );
   fputc( 0x00, tep );
   fputc( 0x01, tep );   // bề dài dữ liệu
   fputc( 0x00, tep );
   fputc( 0x00, tep );
   fputc( 0x00, tep );
   
   fputc( 0x03, tep );  // ZIP
}

void luuThongTinCuaSoDuLieu( FILE *tep, unsigned int beRong, unsigned int beCao ) {
   beRong--;  // số cột cuối
   beCao--;   // số hàng cuối
   fprintf( tep, "dataWindow" );
   fputc( 0x00, tep );
   fprintf( tep, "box2i" );
   fputc( 0x00, tep );
   fputc( 16, tep );
   fputc( 0x00, tep );
   fputc( 0x00, tep );
   fputc( 0x00, tep );
   
   // ---- góc x
   fputc( 0x00, tep );
   fputc( 0x00, tep );
   fputc( 0x00, tep );
   fputc( 0x00, tep );
   // ---- góc y
   fputc( 0x00, tep );
   fputc( 0x00, tep );
   fputc( 0x00, tep );
   fputc( 0x00, tep );
   // ---- cột cuối
   fputc( beRong & 0xff, tep );
   fputc( (beRong >> 8) & 0xff, tep );
   fputc( 0x00, tep );
   fputc( 0x00, tep );
   // ---- hàng cuối
   fputc( beCao & 0xff, tep );
   fputc( (beCao >> 8), tep );
   fputc( 0x00, tep );
   fputc( 0x00, tep );
}

void luuThongTinCuaSoChieu( FILE *tep, unsigned int beRong, unsigned int beCao ) {
   beRong--;  // số cột cuối
   beCao--;   // số hàng cuối
   fprintf( tep, "displayWindow" );
   fputc( 0x00, tep );
   fprintf( tep, "box2i" );
   fputc( 0x00, tep );
   fputc( 16, tep );
   fputc( 0x00, tep );
   fputc( 0x00, tep );
   fputc( 0x00, tep );
   
   // ---- góc x
   fputc( 0x00, tep );
   fputc( 0x00, tep );
   fputc( 0x00, tep );
   fputc( 0x00, tep );
   // ---- góc y
   fputc( 0x00, tep );
   fputc( 0x00, tep );
   fputc( 0x00, tep );
   fputc( 0x00, tep );
   // ---- cột cuối
   fputc( beRong & 0xff, tep );
   fputc( (beRong >> 8) & 0xff, tep );
   fputc( 0x00, tep );
   fputc( 0x00, tep );
   // ---- hàng cuối
   fputc( beCao & 0xff, tep );
   fputc( (beCao >> 8), tep );
   fputc( 0x00, tep );
   fputc( 0x00, tep );
}

void luuThongTinThuTuHang( FILE *tep, unsigned char thuTu ) {
   // ---- thứ tự hàng
   fprintf( tep, "lineOrder" );
   fputc( 0x00, tep );
   fprintf( tep, "lineOrder" );
   fputc( 0x00, tep );
   fputc( 0x01, tep );   // bề dài dữ liệu
   fputc( 0x00, tep );
   fputc( 0x00, tep );
   fputc( 0x00, tep );
   
   fputc( 0x00, tep );  // từ nhỏ tới lớn
}

void luuThongTinDiemAnh( FILE *tep ) {

   // ---- tỉ số cạnh điểm ảnh
   fprintf( tep, "pixelAspectRatio" );
   fputc( 0x00, tep );
   fprintf( tep, "float" );
   fputc( 0x00, tep );
   fputc( 0x04, tep );   // bề dài dữ liệu
   fputc( 0x00, tep );
   fputc( 0x00, tep );
   fputc( 0x00, tep );
   
   fputc( 0x00, tep );  // 1.0
   fputc( 0x00, tep );
   fputc( 0x80, tep );
   fputc( 0x3f, tep );
}

void luuThongTinManChieu( FILE *tep ) {

   // ---- trung tâm cửa sổ màn
   fprintf( tep, "screenWindowCenter" );
   fputc( 0x00, tep );
   fprintf( tep, "v2f" );
   fputc( 0x00, tep );
   fputc( 0x08, tep );   // bề dài dữ liệu
   fputc( 0x00, tep );
   fputc( 0x00, tep );
   fputc( 0x00, tep );
   // ---- tọa độ x (hoành độ)
   fputc( 0x00, tep );
   fputc( 0x00, tep );
   fputc( 0x00, tep );
   fputc( 0x00, tep );
   // ---- tọa độ y (tung độ)
   fputc( 0x00, tep );
   fputc( 0x00, tep );
   fputc( 0x00, tep );
   fputc( 0x00, tep );
   
   // ---- bề rộng cửa sổ màn
   fprintf( tep, "screenWindowWidth" );
   fputc( 0x00, tep );
   fprintf( tep, "float" );
   fputc( 0x00, tep );
   fputc( 0x04, tep );   // bề dài dữ liệu
   fputc( 0x00, tep );
   fputc( 0x00, tep );
   fputc( 0x00, tep );
   
   fputc( 0x00, tep );   // 1.0f
   fputc( 0x00, tep );
   fputc( 0x80, tep );
   fputc( 0x3f, tep );
}

void luuThongTinOChuNhat( FILE *tep, unsigned int beRongO, unsigned int beCaoO, unsigned char soLuongTang ) {

   // ---- tỉ số cạnh điểm ảnh
   fprintf( tep, "tiles" );
   fputc( 0x00, tep );
   fprintf( tep, "tiledesc" );
   fputc( 0x00, tep );
   fputc( 9, tep );
   fputc( 0x00, tep );
   fputc( 0x00, tep );
   fputc( 0x00, tep );
   // ---- bề rộng ô
   fputc( beRongO & 0xff, tep );
   fputc( (beRongO >> 8) & 0xff, tep );
   fputc( (beRongO >> 16) & 0xff, tep );
   fputc( (beRongO >> 24) & 0xff, tep );
   // ---- bề cao ô
   fputc( beCaoO & 0xff, tep );
   fputc( (beCaoO >> 8) & 0xff, tep );
   fputc( (beCaoO >> 16) & 0xff, tep );
   fputc( (beCaoO >> 24) & 0xff, tep );
   // ---- số lượng tầng
   fputc( soLuongTang, tep );
}

void luuOChuNhatZIP( char *tenTep, ThongTinOChuNhat *thongTinOChuNhat, unsigned char kieuDuLieu ) {
   
   unsigned short soLuongCot_O = thongTinOChuNhat->cotKetThuc - thongTinOChuNhat->cotBatDau;
   unsigned short soLuongHang_O = thongTinOChuNhat->hangKetThuc - thongTinOChuNhat->hangBatDau;
   
   unsigned int diaChiDem = 0;
   unsigned int diaChiKenh = 0;
   
   unsigned int soLuongDiemAnh = soLuongCot_O * soLuongHang_O;
   
   unsigned char *dem = thongTinOChuNhat->thongTinGom;

   // ---- lưu dữ liệu cho thành phần ảnh
   unsigned short soHang = 0;
   while( soHang < soLuongHang_O ) {
   // ---- tùy kiểu dữ liệu trong ảnh
      if( kieuDuLieu == kKIEU_FLOAT ) {
         // ---- chép kênh xanh
         chepDuLieuKenhFloat( &(dem[diaChiDem]), &(thongTinOChuNhat->kenhXanh[diaChiKenh]), soLuongCot_O );
         // ---- chép kênh lục
         diaChiDem += soLuongCot_O << kieuDuLieu;
         chepDuLieuKenhFloat( &(dem[diaChiDem]), &(thongTinOChuNhat->kenhLuc[diaChiKenh]), soLuongCot_O );
         // ---- chép kênh đỏ
         diaChiDem += soLuongCot_O << kieuDuLieu;
         chepDuLieuKenhFloat( &(dem[diaChiDem]), &(thongTinOChuNhat->kenhDo[diaChiKenh]), soLuongCot_O );
         // ---- tiếp theo
         diaChiDem += soLuongCot_O << kieuDuLieu;
         diaChiKenh += soLuongCot_O;
      }
      else {  // kKIEU_HALF
         // ---- chép kênh xanh
         chepDuLieuKenhHalf( &(dem[diaChiDem]), &(thongTinOChuNhat->kenhXanh[diaChiKenh]), soLuongCot_O );
         // ---- chép kênh lục
         diaChiDem += soLuongCot_O << kieuDuLieu;
         chepDuLieuKenhHalf( &(dem[diaChiDem]), &(thongTinOChuNhat->kenhLuc[diaChiKenh]), soLuongCot_O );
         // ---- chép kênh đỏ
         diaChiDem += soLuongCot_O << kieuDuLieu;
         chepDuLieuKenhHalf( &(dem[diaChiDem]), &(thongTinOChuNhat->kenhDo[diaChiKenh]), soLuongCot_O );
         // ---- tiếp theo
         diaChiDem += soLuongCot_O << kieuDuLieu;
         diaChiKenh += soLuongCot_O;
      }
//      printf("soHang %d  diaChiDem %d  diaChiKenh %d\n", soHang, diaChiDem, diaChiKenh );

      soHang++;
//      printf("   soHang %d  diaChiDem %d\n", soHang, diaChiDem );
   }
   
   // ====
   unsigned int beDaiDem = (soLuongCot_O*soLuongHang_O*3) << kieuDuLieu;

   locDuLieuTrongDem( dem, beDaiDem, thongTinOChuNhat->thongTinLoc);
   unsigned int beDaiDuLieuNen = nenZIP( thongTinOChuNhat->thongTinLoc, beDaiDem, thongTinOChuNhat->thongTinNen, beDaiDem << 1 );
//   printf( "beDaiDem %d  beDaiDuLieuNen %d\n", beDaiDem, beDaiDuLieuNen );
   // ====
   FILE *tep = fopen( tenTep, "rb+" );
   
   if( tep != NULL ) {
      // ---- kèm dữ liệu ô
      fseek( tep, 0, SEEK_END );
      unsigned long long diaChiThanhPhan = ftell( tep );
      
      // ---- số ô x
      unsigned int x = thongTinOChuNhat->x;
      fputc( x & 0xff, tep );
      fputc( (x >> 8) & 0xff, tep );
      fputc( (x >> 16) & 0xff, tep );
      fputc( (x >> 24) & 0xff, tep );
      // ---- số ô y
      unsigned int y = thongTinOChuNhat->y;
      fputc( y & 0xff, tep );
      fputc( (y >> 8) & 0xff, tep );
      fputc( (y >> 16) & 0xff, tep );
      fputc( (y >> 24) & 0xff, tep );
      // ---- độ giải x
      unsigned int doGiai_x = 0;
      fputc( doGiai_x & 0xff, tep );
      fputc( (doGiai_x >> 8) & 0xff, tep );
      fputc( (doGiai_x >> 16) & 0xff, tep );
      fputc( (doGiai_x >> 24) & 0xff, tep );
      // ---- độ giải y
      unsigned int doGiai_y = 0;
      fputc( doGiai_y & 0xff, tep );
      fputc( (doGiai_y >> 8) & 0xff, tep );
      fputc( (doGiai_y >> 16) & 0xff, tep );
      fputc( (doGiai_y >> 24) & 0xff, tep );
      
      // ---- bề dài dữ liệu ô
      fputc( beDaiDuLieuNen & 0xff, tep );
      fputc( (beDaiDuLieuNen >> 8) & 0xff, tep );
      fputc( (beDaiDuLieuNen >> 16) & 0xff, tep );
      fputc( (beDaiDuLieuNen >> 24) & 0xff, tep );
      
      // ---- lưu dữ liệu nén
      unsigned int diaChi = 0;
      
      while( diaChi < beDaiDuLieuNen ) {
         fputc( thongTinOChuNhat->thongTinNen[diaChi], tep );
         //      printf( "%02x ", thongTinOChuNhat->thongTinNen[diaChi] );
         diaChi++;
      }
      
      // ---- lưu địa chỉ trong bảng thành phân
      fseek( tep, thongTinOChuNhat->viTriTrongBangThanhPhan, SEEK_SET );
      //   printf( "\nviTriTrongBangThanhPhan %ld  diaChiThanhPhan %ld\n", thongTinOChuNhat->viTriTrongBangThanhPhan, diaChiThanhPhan );
      
      fputc( diaChiThanhPhan & 0xff, tep );
      fputc( (diaChiThanhPhan >> 8), tep );
      fputc( (diaChiThanhPhan >> 16), tep );
      fputc( (diaChiThanhPhan >> 24), tep );
      fputc( (diaChiThanhPhan >> 32), tep );
      fputc( (diaChiThanhPhan >> 40), tep );
      fputc( (diaChiThanhPhan >> 48), tep );
      fputc( (diaChiThanhPhan >> 56), tep );
      
      thongTinOChuNhat->viTriTrongBangThanhPhan += 8;
      fclose( tep );
   }
   else {
      printf( "SAI LẦM LƯU TẬP TIN\n" );
      exit(1);
   }
}

void luuThoiGianKetXuat( FILE *tep, unsigned short thoiGianKetXuat ) {

   // ---- tỉ số cạnh điểm ảnh
   fprintf( tep, "RenderTime" );
   fputc( 0x00, tep );
   fprintf( tep, "string" );
   fputc( 0x00, tep );


   // ---- phút
   unsigned short phut = thoiGianKetXuat/60;
   unsigned short giay = thoiGianKetXuat - phut*60;
   if( phut < 60 ) {
      fputc( 0x08, tep );   // bề dài dữ liệu
      fputc( 0x00, tep );
      fputc( 0x00, tep );
      fputc( 0x00, tep );
      fprintf( tep, "%02d", phut );   // đổi thành thập phận
   }
   else {
      // ---- giờ
     unsigned short gio = phut/60;
      phut -= gio*60;
      fputc( 0x0b, tep );   // bề dài dữ liệu
      fputc( 0x00, tep );
      fputc( 0x00, tep );
      fputc( 0x00, tep );
      gio = phut/60;
      fprintf( tep, "%02d:%02d", gio, phut );   // đổi thành thập phận
   }

   // ---- giây
   fprintf( tep, ":%02d", giay );  // đổi thành thập phận
   fputc( '.', tep );
   fputc( '0', tep );
   fputc( '0', tep );
}

void luuBangDuLieuAnh( FILE *tep, unsigned short soLuongThanhPhan ) {
   
   // ---- chưa biết địa chỉ thành phần vì chưa nén dữ liệu, sau nén sẽ đặt gía trị trong bảng  
   unsigned long long diaChiThanhPhan = 0x0;

   unsigned short soThanhPhan = 0;
   while( soThanhPhan < soLuongThanhPhan ) {
      fputc( diaChiThanhPhan & 0xff, tep );
      fputc( (diaChiThanhPhan >> 8), tep );
      fputc( (diaChiThanhPhan >> 16), tep );
      fputc( (diaChiThanhPhan >> 24), tep );
      fputc( (diaChiThanhPhan >> 32), tep );
      fputc( (diaChiThanhPhan >> 40), tep );
      fputc( (diaChiThanhPhan >> 48), tep );
      fputc( (diaChiThanhPhan >> 56), tep );
      soThanhPhan++;
   }

}

#pragma mark ---- CHÉP DỮ LIỆU
void chepDuLieuKenhFloat( unsigned char *dem, const float *kenh, unsigned short beRong ) {

   unsigned short soCot = 0;
   unsigned int diaChiDem = 0;
   while( soCot < beRong ) {

      union {
         unsigned int i;
         float f;
      } u_if;

      u_if.f = kenh[soCot];
      dem[diaChiDem] = u_if.i & 0xff;
      dem[diaChiDem + 1] = (u_if.i >> 8) & 0xff;
      dem[diaChiDem + 2] = (u_if.i >> 16) & 0xff;
      dem[diaChiDem + 3] = (u_if.i >> 24) & 0xff;
      diaChiDem += 4;
      soCot++;
   }

}

void chepDuLieuKenhHalf( unsigned char *dem, const float *kenh, unsigned short beRong ) {
   
   unsigned short soCot = 0;
   unsigned int diaChiDem = 0;
   while( soCot < beRong ) {

      unsigned short h = doiFloatSangHalf( kenh[soCot] );
      dem[diaChiDem] = h & 0xff;
      dem[diaChiDem + 1] = (h >> 8) & 0xff;
      diaChiDem += 2;
      soCot++;
   }

}

#pragma mark ---- ĐỔI FLOAT
// ---- rất đơn giản, cho tốc đ`ộ nhanh và biết nguồn dữ liệu, KHÔNG THEO TOÀN CHUẨN EXR cho đổi float
// giá trị nào âm, NaN, ∞ đặt = 0,0
unsigned short doiFloatSangHalf( float soFloat ) {
 
   union {
      float f;
      unsigned int i;
   } ui;

   ui.f = soFloat;

   unsigned short soHalf;
   if( ui.i & 0x80000000 )   // âm
      soHalf = 0x0000;
   else if( (ui.f == 0.0f) || (ui.f == -0.0f) ) { // số không
      soHalf = 0x0000;
   }
   else {
      int muFloat = ui.i & 0x7f800000;

      if( muFloat == 0x7f800000 )  // NaN
         soHalf = 0x0000;
      else {
         char muKiemTra = (muFloat >> 23) - 127;

         if( muKiemTra < -14 ) // qúa nhỏ
            soHalf = 0x0000;
         else if( muKiemTra > 15 )
            soHalf = 0x7c00;  //+∞
         else {  // bình thường
            unsigned short muHalf = (((muFloat & 0x7f800000) >> 23) - 112) << 10;
            unsigned int dinhTriFloat = ui.i & 0x007fffff;
            unsigned short dinhTriHalf = ((dinhTriFloat + 0x00000fff + ((dinhTriFloat >> 13) & 1)) >> 13);
            soHalf = muHalf + dinhTriHalf;
         }
      }
   }

   return soHalf;
}

// ---- Từ thư viện OpenEXR
void locDuLieuTrongDem(unsigned char *dem, unsigned int beDai, unsigned char *demLoc ) {

   {
      unsigned char *t1 = demLoc;
      unsigned char *t2 = demLoc + (beDai + 1) / 2;
      char *dau = (char *)dem;  // đầu đệm cần lọc
      char *ketThuc = dau + beDai;
      unsigned char xong = kSAI;
      
      while( !xong )
      {
         if (dau < ketThuc)
            *(t1++) = *(dau++);
         else
            xong = kSAI;
         
         if (dau < ketThuc)
            *(t2++) = *(dau++);
         else
            xong = kDUNG;
      }
   }
   
   // trừ
   {
      unsigned char *t = (unsigned char *)demLoc + 1;
      unsigned char *ketThuc = (unsigned char *)demLoc + beDai;
      int p = t[-1];
      
      while (t < ketThuc) {
         
         int d = (int)(t[0]) - p + 128;
         p = t[0];
         t[0] = d;
         ++t;
      }
   }

}

// ---- Từ thư viện OpenEXR
unsigned int nenZIP(unsigned char *dem, int beDaiDem, unsigned char *demNen, int beDaiDemNen ) {
   
   int err;
   z_stream dongNen; // dòng nén
   
   dongNen.zalloc = Z_NULL;
   dongNen.zfree = Z_NULL;
   dongNen.opaque = Z_NULL;
   
   
   // ---- kiểm tra sai có lầm khởi đầu
   err = deflateInit(&dongNen, Z_DEFAULT_COMPRESSION);
   
   if( err != Z_OK )
      printf( "nenZip: Vấn đề khởi đầu nén %d (%x) dongNen.avail_in %d", err, err, dongNen.avail_in );
   
   // ---- cho dữ liệu cần nén
   dongNen.next_in = dem;
   dongNen.avail_in = beDaiDem;
   
   // ---- xem nếu đệm có thể chứa dữ liệu nén  
   unsigned int beDaiDuDoan = (unsigned int)deflateBound(&dongNen, beDaiDem );
   if( beDaiDuDoan > beDaiDemNen )
      printf( "nenZIP: dự đoán beDaiDuDoan %d > đệm chứa beDaiDemNen %d", beDaiDuDoan, beDaiDemNen );
   
   // ---- cho đệm cho chứa dữ liệu nén
   dongNen.next_out  = demNen;
   dongNen.avail_out = beDaiDemNen;  // bề dải đệm chứa dữ liệu nén
   err = deflate(&dongNen, Z_FINISH);
   
   if( err != Z_STREAM_END ) {
      if( err == Z_OK) {
         printf( "nenZIP: Z_OK d_stream.avail_out %d d_stream.total_out %lu",
               dongNen.avail_out, dongNen.total_out );
      }
      else
         printf( "nenZIP: sai lầm hết dữ liệu sớm hơn ý định, deflate %d (%x) d_stream.avail_out %d d_stream.total_out %lu",
               err, err, dongNen.avail_in, dongNen.total_in );
   }
   
   err = deflateEnd( &dongNen );
   if( err != Z_OK )
      printf( "nenZIP: Sai lầm deflateEnd %d (%x) dongNen.avail_out %d", err, err, dongNen.avail_out );

   return dongNen.total_out;
}
