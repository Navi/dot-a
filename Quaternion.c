// Quaternion.h
// 2558-12-16

#include <math.h>
#include "Quaternion.h"

Quaternion datQuaternionTuVectoVaGocQuay( Vecto *vecto, float gocQuay ) {
   
   donViHoa( vecto );
   // ---- nữa góc quay (âm cho quay theo qũy tắc tay phải)
   float nuaGocQuay = -gocQuay*0.5f;  // /banKinh;
   float sinNuaGocQuay = sinf( nuaGocQuay );
   // ---- tính quaternion
   Quaternion quaternion;
   quaternion.w = cosf( nuaGocQuay );
   quaternion.x = vecto->x*sinNuaGocQuay;
   quaternion.y = vecto->y*sinNuaGocQuay;
   quaternion.z = vecto->z*sinNuaGocQuay;

   return quaternion;
}

void quaternionSangMaTran( Quaternion *quaternion, float *maTran ) {
   
   float doLonBinhPhong = quaternion->w*quaternion->w + quaternion->x*quaternion->x + quaternion->y*quaternion->y + quaternion->z*quaternion->z;
	maTran[0] = doLonBinhPhong - 2.0f*(quaternion->y*quaternion->y + quaternion->z*quaternion->z);
	maTran[1] = 2.0f*(quaternion->x*quaternion->y - quaternion->w*quaternion->z);
	maTran[2] = 2.0f*(quaternion->x*quaternion->z + quaternion->w*quaternion->y);
   maTran[3] = 0.0f;
   
   maTran[4] = 2.0f*(quaternion->x*quaternion->y + quaternion->w*quaternion->z);
   maTran[5] = doLonBinhPhong - 2.0f*(quaternion->x*quaternion->x + quaternion->z*quaternion->z);
   maTran[6] = 2.0f*(quaternion->y*quaternion->z - quaternion->w*quaternion->x);
   maTran[7] = 0.0f;
   
   maTran[8] = 2.0f*(quaternion->x*quaternion->z - quaternion->w*quaternion->y);
	maTran[9] = 2.0f*(quaternion->y*quaternion->z + quaternion->w*quaternion->x);
	maTran[10] = doLonBinhPhong - 2.0f*(quaternion->x*quaternion->x + quaternion->y*quaternion->y);
   maTran[11] = 0.0f;
   
   maTran[12] = 0.0f;
   maTran[13] = 0.0f;
   maTran[14] = 0.0f;
   maTran[15] = 1.0f;
   
   // đơn vị hóa maTran
	maTran[0] /= doLonBinhPhong;
	maTran[1] /= doLonBinhPhong;
	maTran[2] /= doLonBinhPhong;
   
	maTran[4] /= doLonBinhPhong;
	maTran[5] /= doLonBinhPhong;
	maTran[6] /= doLonBinhPhong;
   
	maTran[8] /= doLonBinhPhong;
	maTran[9] /= doLonBinhPhong;
	maTran[10] /= doLonBinhPhong;
}

Quaternion nhanQuaternionVoiQuaternion( Quaternion *quaternion0, Quaternion *quaternion1 ) {
   
   Quaternion quaternionKetQua;
   // ---- nhân quaternion0 * quaternion1
   quaternionKetQua.w = quaternion0->w*quaternion1->w - quaternion0->x*quaternion1->x - quaternion0->y*quaternion1->y - quaternion0->z*quaternion1->z;
   quaternionKetQua.x = quaternion0->w*quaternion1->x + quaternion0->x*quaternion1->w + quaternion0->y*quaternion1->z - quaternion0->z*quaternion1->y;
   quaternionKetQua.y = quaternion0->w*quaternion1->y - quaternion0->x*quaternion1->z + quaternion0->y*quaternion1->w + quaternion0->z*quaternion1->x;
	quaternionKetQua.z = quaternion0->w*quaternion1->z + quaternion0->x*quaternion1->y - quaternion0->y*quaternion1->x + quaternion0->z*quaternion1->w;

   return quaternionKetQua;
}


Quaternion tinhXoayChoVatThe( Quaternion *quaternionVatThe, Vecto trucXoay, float tocDo, float banKinh ) {
   
   float gocXoay = tocDo/banKinh;
   Quaternion quaternionXoay = datQuaternionTuVectoVaGocQuay( &trucXoay, gocXoay );
   Quaternion xoayMoi = nhanQuaternionVoiQuaternion( quaternionVatThe, &quaternionXoay );
   return xoayMoi;
}
