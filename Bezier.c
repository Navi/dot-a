#include "Bezier.h"

#pragma mark ---- Bezier
Vecto tinhViTriBezier3C( Bezier *bezier, float thamSo ) {
   
   // ---- mức 0, cần tính 3 điểm
   Vecto muc0[3];
   muc0[0].x = (1.0f - thamSo)*bezier->diemQuanTri[0].x + thamSo*bezier->diemQuanTri[1].x;
   muc0[0].y = (1.0f - thamSo)*bezier->diemQuanTri[0].y + thamSo*bezier->diemQuanTri[1].y;
   muc0[0].z = (1.0f - thamSo)*bezier->diemQuanTri[0].z + thamSo*bezier->diemQuanTri[1].z;
   muc0[1].x = (1.0f - thamSo)*bezier->diemQuanTri[1].x + thamSo*bezier->diemQuanTri[2].x;
   muc0[1].y = (1.0f - thamSo)*bezier->diemQuanTri[1].y + thamSo*bezier->diemQuanTri[2].y;
   muc0[1].z = (1.0f - thamSo)*bezier->diemQuanTri[1].z + thamSo*bezier->diemQuanTri[2].z;
   muc0[2].x = (1.0f - thamSo)*bezier->diemQuanTri[2].x + thamSo*bezier->diemQuanTri[3].x;
   muc0[2].y = (1.0f - thamSo)*bezier->diemQuanTri[2].y + thamSo*bezier->diemQuanTri[3].y;
   muc0[2].z = (1.0f - thamSo)*bezier->diemQuanTri[2].z + thamSo*bezier->diemQuanTri[3].z;
   
   // ---- mức 1, cần tính 2 điểm
   Vecto muc1[2];
   muc1[0].x = (1.0f - thamSo)*muc0[0].x + thamSo*muc0[1].x;
   muc1[0].y = (1.0f - thamSo)*muc0[0].y + thamSo*muc0[1].y;
   muc1[0].z = (1.0f - thamSo)*muc0[0].z + thamSo*muc0[1].z;
   muc1[1].x = (1.0f - thamSo)*muc0[1].x + thamSo*muc0[2].x;
   muc1[1].y = (1.0f - thamSo)*muc0[1].y + thamSo*muc0[2].y;
   muc1[1].z = (1.0f - thamSo)*muc0[1].z + thamSo*muc0[2].z;
   
   Vecto viTri;
   viTri.x = (1.0f - thamSo)*muc1[0].x + thamSo*muc1[1].x;
   viTri.y = (1.0f - thamSo)*muc1[0].y + thamSo*muc1[1].y;
   viTri.z = (1.0f - thamSo)*muc1[0].z + thamSo*muc1[1].z;

   return viTri;
}

// ds(t)/dt = -x0*3*(1-t)^2 + x1*3*[(1-t)^2 - 2*t(1-t)] + x2*3[2*t(1-t) - t^2] + x3*3*t^2
// ds(t)/dt = 3*(1-t) * [-x0*(1-t) + x1*(1-3t)]    +    3*t * [x2*(2-3t) + x3*t]
Vecto tinhVanTocBezier3C( Bezier *bezier, float thamSo ) {
   
   // ---- mức 0, cần tính 3 điểm
   Vecto muc0[2];
   muc0[0].x = -(1.0f - thamSo)*bezier->diemQuanTri[0].x + (1.0f - 3.0f*thamSo)*bezier->diemQuanTri[1].x;
   muc0[0].y = -(1.0f - thamSo)*bezier->diemQuanTri[0].y + (1.0f - 3.0f*thamSo)*bezier->diemQuanTri[1].y;
   muc0[0].z = -(1.0f - thamSo)*bezier->diemQuanTri[0].z + (1.0f - 3.0f*thamSo)*bezier->diemQuanTri[1].z;
   muc0[1].x = (2.0f - 3.0f*thamSo)*bezier->diemQuanTri[2].x + thamSo*bezier->diemQuanTri[3].x;
   muc0[1].y = (2.0f - 3.0f*thamSo)*bezier->diemQuanTri[2].y + thamSo*bezier->diemQuanTri[3].y;
   muc0[1].z = (2.0f - 3.0f*thamSo)*bezier->diemQuanTri[2].z + thamSo*bezier->diemQuanTri[3].z;

   Vecto vanToc;
   vanToc.x = (1.0f - thamSo)*muc0[0].x + thamSo*muc0[1].x;
   vanToc.y = (1.0f - thamSo)*muc0[0].y + thamSo*muc0[1].y;
   vanToc.z = (1.0f - thamSo)*muc0[0].z + thamSo*muc0[1].z;

   vanToc.x *= 3.0f;
   vanToc.y *= 3.0f;
   vanToc.z *= 3.0f;
   
   return vanToc;
}

// ds^2(t)/dt^2 = 6*x0*(1-t) + 3*x1*[-4*(1-t) + 2*t] + 3*x2[2*(1-t) - 4*t] + 6*x3*t
// ds^2(t)/dt^2 = (6*x0 - 12*x1 + 6*x2)*(1-t) + (6*x1 - 12*x2 + 6*x3)*t
Vecto tinhGiaTocBezier3C( Bezier *bezier, float thamSo ) {
   
   // ---- mức 0, cần tính 3 điểm
   Vecto giaToc;
   giaToc.x = (bezier->diemQuanTri[0].x - 2.0f*bezier->diemQuanTri[1].x + bezier->diemQuanTri[2].x)*(1.0f - thamSo);
   giaToc.y = (bezier->diemQuanTri[0].y - 2.0f*bezier->diemQuanTri[1].y + bezier->diemQuanTri[2].y)*(1.0f - thamSo);
   giaToc.z = (bezier->diemQuanTri[0].z - 2.0f*bezier->diemQuanTri[1].z + bezier->diemQuanTri[2].z)*(1.0f - thamSo);
   giaToc.x += (bezier->diemQuanTri[1].x - 2.0f*bezier->diemQuanTri[2].x + bezier->diemQuanTri[3].x)*thamSo;
   giaToc.y += (bezier->diemQuanTri[1].y - 2.0f*bezier->diemQuanTri[2].y + bezier->diemQuanTri[3].y)*thamSo;
   giaToc.z += (bezier->diemQuanTri[1].z - 2.0f*bezier->diemQuanTri[2].z + bezier->diemQuanTri[3].z)*thamSo;
   
   giaToc.x *= 6.0f;
   giaToc.y *= 6.0f;
   giaToc.z *= 6.0f;
   
   return giaToc;
}
