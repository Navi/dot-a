#include "DocThamSo.h"
#include <stdio.h>
#include <stdlib.h>
#include "HangSo.h"

#pragma mark ---- Đọc Tham Số
void docThamSoPhimTruong( int argc, char **argv, unsigned int *soPhimTruong ) {
   
   if( argc > 1 )
      sscanf( argv[1], "%u", soPhimTruong );
   
   else {
      printf( "Lệnh Ví Dụ: <sốPhimTrường> <sốHoạtHìnhĐầu> <sốHoạtHìnhCuối> <bềRộngẢnh> <bềCaoẢnh> <cáchGiữaĐiểmẢnh>\n" );
      printf( "    Ví Dụ:./doTia 0 0 10 4096 2160 0.0015\n" );
      exit(0);
   }
}

void docThamSoHoatHinh( int argc, char **argv, unsigned int *soHoatHinhDau, unsigned int *soHoatHinhCuoi ) {

   if( argc > 3 ) {
      sscanf( argv[2], "%u", soHoatHinhDau );
      sscanf( argv[3], "%u", soHoatHinhCuoi );
      // ---- kiểm tra soHoatHinhDau nhỏ soHoatHinhCuoi
      if( *soHoatHinhDau >= *soHoatHinhCuoi )
         *soHoatHinhCuoi = *soHoatHinhDau + 1;
   }
   else if ( argc > 2 ){
      sscanf( argv[2], "%u", soHoatHinhDau );
      *soHoatHinhCuoi = *soHoatHinhDau + 1;
   }
   else {
      *soHoatHinhDau = 0;
   }
}

void docThamCoKich( int argc, char **argv, unsigned int *beRong, unsigned int *beCao, float *coThuocDiemAnh ) {
   
   if( argc > 6 ) {
      sscanf( argv[4], "%u", beRong );
      sscanf( argv[5], "%u", beCao );
      sscanf( argv[6], "%f", coThuocDiemAnh );
      // ---- kiểm tra > 100
      if( *beRong == 0 )
         *beRong = 1;
      else if( *beRong > 8192 )
         *beRong = 8192;
      
      if( *beCao == 0 )
         *beCao = 1;
      else if( *beCao > 8192 )
         *beCao = 8192;
      
      if( *coThuocDiemAnh < 0.00001f )
         *coThuocDiemAnh = 0.00001f;
      else if( *coThuocDiemAnh > 5.0f )
         *coThuocDiemAnh = 5.0f;
   }
   else {
      *beRong = 200;
      *beCao = 100;
      *coThuocDiemAnh = 0.03f;
   }
   
}

void docThamXaiO( int argc, char **argv, unsigned char *xaiO, unsigned short *beRongO, unsigned short *beCaoO, unsigned char *tinhTiep ) {

   if( argc > 7 ) {
      *xaiO = kDUNG;
      char kyTu;
      sscanf( argv[7], "%c", &kyTu );
      if( kyTu == 't' )
         *tinhTiep = kDUNG;  // tính tiếp
      else
         *tinhTiep = kSAI;  // mới bắt đầu tính
      
   }
   else
      *xaiO = kSAI;
   
   if( argc > 9 ) {
      unsigned int beRongO_int;
      unsigned int beCaoO_int;
      sscanf( argv[8], "%u", &beRongO_int );
      sscanf( argv[9], "%u", &beCaoO_int );
      
      *beRongO = (unsigned short)beRongO_int;
      *beCaoO = (unsigned short)beCaoO_int;
   }
   else {
      printf( "Không có kích cỡ ô, Đặt bề rộng ô = 128, bề cao = 128 điểm ảnh\n" );
      *beRongO = 128;
      *beCaoO = 128;
   }
}
