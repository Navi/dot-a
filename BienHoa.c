#include "BienHoa.h"


#pragma mark ---- Biến Hóa (Ma Trận)
void datDonVi( float *maTran ) {
   
   maTran[0] = 1.0f;
   maTran[1] = 0.0f;
   maTran[2] = 0.0f;
   maTran[3] = 0.0f;
   
   maTran[4] = 0.0f;
   maTran[5] = 1.0f;
   maTran[6] = 0.0f;
   maTran[7] = 0.0f;
   
   maTran[8] = 0.0f;
   maTran[9] = 0.0f;
   maTran[10] = 1.0f;
   maTran[11] = 0.0f;
   
   maTran[12] = 0.0f;
   maTran[13] = 0.0f;
   maTran[14] = 0.0f;
   maTran[15] = 1.0f;
};

void datPhongTo( float *maTran, float phongToX, float phongToY, float phongToZ ) {
   
   maTran[0] = phongToX;
   maTran[1] = 0.0f;
   maTran[2] = 0.0f;
   maTran[3] = 0.0f;
   
   maTran[4] = 0.0f;
   maTran[5] = phongToY;
   maTran[6] = 0.0f;
   maTran[7] = 0.0f;
   
   maTran[8] = 0.0f;
   maTran[9] = 0.0f;
   maTran[10] = phongToZ;
   maTran[11] = 0.0f;
   
   maTran[12] = 0.0f;
   maTran[13] = 0.0f;
   maTran[14] = 0.0f;
   maTran[15] = 1.0f;
};

void datDich( float *maTran, float x, float y, float z ) {
   
   maTran[0] = 1.0f;
   maTran[1] = 0.0f;
   maTran[2] = 0.0f;
   maTran[3] = 0.0f;
   
   maTran[4] = 0.0f;
   maTran[5] = 1.0f;
   maTran[6] = 0.0f;
   maTran[7] = 0.0f;
   
   maTran[8] = 0.0f;
   maTran[9] = 0.0f;
   maTran[10] = 1.0f;
   maTran[11] = 0.0f;
   
   maTran[12] = x;
   maTran[13] = y;
   maTran[14] = z;
   maTran[15] = 1.0f;
};

void latMaTran4x4( float *maTran ) {
   float giaTri;
   giaTri = maTran[1];
   maTran[1] = maTran[4];
   maTran[4] = giaTri;
   // ----
   giaTri = maTran[2];
   maTran[2] = maTran[8];
   maTran[8] = giaTri;
   // ----
   giaTri = maTran[3];
   maTran[3] = maTran[12];
   maTran[12] = giaTri;
   // ----
   giaTri = maTran[6];
   maTran[6] = maTran[9];
   maTran[9] = giaTri;
   // ----
   giaTri = maTran[7];
   maTran[7] = maTran[13];
   maTran[13] = giaTri;
   // ----
   giaTri = maTran[11];
   maTran[11] = maTran[14];
   maTran[14] = giaTri;
}

Vecto nhanVectoVoiMaTran3x3( Vecto *vecto, float *maTran ) {
   
   Vecto ketQua;
   ketQua.x = vecto->x*maTran[0] + vecto->y*maTran[4] + vecto->z*maTran[8];
   ketQua.y = vecto->x*maTran[1] + vecto->y*maTran[5] + vecto->z*maTran[9];
   ketQua.z = vecto->x*maTran[2] + vecto->y*maTran[6] + vecto->z*maTran[10];
   
   return ketQua;
}

Vecto nhanVectoVoiMaTran4x4( Vecto *vecto, float *maTran ) {
   
   Vecto ketQua;
   ketQua.x = vecto->x*maTran[0] + vecto->y*maTran[4] + vecto->z*maTran[8] + maTran[12];
   ketQua.y = vecto->x*maTran[1] + vecto->y*maTran[5] + vecto->z*maTran[9] + maTran[13];
   ketQua.z = vecto->x*maTran[2] + vecto->y*maTran[6] + vecto->z*maTran[10] + maTran[14];
   
   return ketQua;
}

void nhanMaTranVoiMaTran( float *maTranKetQua, float *maTran1, float *maTran2 ) {
   
   // ---- giũ kết quả riêng cho có thể bỏ kết qủa vào maTran1 hay maTran2
   float ketQua00 = maTran1[0]*maTran2[0] + maTran1[1]*maTran2[4] + maTran1[2]*maTran2[8] + maTran1[3]*maTran2[12];
   float ketQua01 = maTran1[0]*maTran2[1] + maTran1[1]*maTran2[5] + maTran1[2]*maTran2[9] + maTran1[3]*maTran2[13];
   float ketQua02 = maTran1[0]*maTran2[2] + maTran1[1]*maTran2[6] + maTran1[2]*maTran2[10] + maTran1[3]*maTran2[14];
   float ketQua03 = maTran1[0]*maTran2[3] + maTran1[1]*maTran2[7] + maTran1[2]*maTran2[11] + maTran1[3]*maTran2[15];
   
   float ketQua10 = maTran1[4]*maTran2[0] + maTran1[5]*maTran2[4] + maTran1[6]*maTran2[8] + maTran1[7]*maTran2[12];
   float ketQua11 = maTran1[4]*maTran2[1] + maTran1[5]*maTran2[5] + maTran1[6]*maTran2[9] + maTran1[7]*maTran2[13];
   float ketQua12 = maTran1[4]*maTran2[2] + maTran1[5]*maTran2[6] + maTran1[6]*maTran2[10] + maTran1[7]*maTran2[14];
   float ketQua13 = maTran1[4]*maTran2[3] + maTran1[5]*maTran2[7] + maTran1[6]*maTran2[11] + maTran1[7]*maTran2[15];
   
   float ketQua20 = maTran1[8]*maTran2[0] + maTran1[9]*maTran2[4] + maTran1[10]*maTran2[8] + maTran1[11]*maTran2[12];
   float ketQua21 = maTran1[8]*maTran2[1] + maTran1[9]*maTran2[5] + maTran1[10]*maTran2[9] + maTran1[11]*maTran2[13];
   float ketQua22 = maTran1[8]*maTran2[2] + maTran1[9]*maTran2[6] + maTran1[10]*maTran2[10] + maTran1[11]*maTran2[14];
   float ketQua23 = maTran1[8]*maTran2[3] + maTran1[9]*maTran2[7] + maTran1[10]*maTran2[11] + maTran1[11]*maTran2[15];
   
   float ketQua30 = maTran1[12]*maTran2[0] + maTran1[13]*maTran2[4] + maTran1[14]*maTran2[8] + maTran1[15]*maTran2[12];
   float ketQua31 = maTran1[12]*maTran2[1] + maTran1[13]*maTran2[5] + maTran1[14]*maTran2[9] + maTran1[15]*maTran2[13];
   float ketQua32 = maTran1[12]*maTran2[2] + maTran1[13]*maTran2[6] + maTran1[14]*maTran2[10] + maTran1[15]*maTran2[14];
   float ketQua33 = maTran1[12]*maTran2[3] + maTran1[13]*maTran2[7] + maTran1[14]*maTran2[11] + maTran1[15]*maTran2[15];
   
   maTranKetQua[0] = ketQua00;
   maTranKetQua[1] = ketQua01;
   maTranKetQua[2] = ketQua02;
   maTranKetQua[3] = ketQua03;
   
   maTranKetQua[4] = ketQua10;
   maTranKetQua[5] = ketQua11;
   maTranKetQua[6] = ketQua12;
   maTranKetQua[7] = ketQua13;
   
   maTranKetQua[8] = ketQua20;
   maTranKetQua[9] = ketQua21;
   maTranKetQua[10] = ketQua22;
   maTranKetQua[11] = ketQua23;
   
   maTranKetQua[12] = ketQua30;
   maTranKetQua[13] = ketQua31;
   maTranKetQua[14] = ketQua32;
   maTranKetQua[15] = ketQua33;
}

#pragma mark ---- Định Hướng Bằng Vectơ Nhìn
// ---- này giả sự không nghiêng nhín thẳng lên hay thẳng xuống, khác (0; ±1; 0)
void dinhHuongMaTranBangVectoNhin( float *maTran, Vecto *vectoNhin ) {
   /* 
   float dau = vectoNhin->y >= 0.0f ? 1.0f : -1.0f;

   float a = -1.0f/(dau + vectoNhin->y);
   float b = vectoNhin->x*vectoNhin->z * a;

   Vecto trucY;
   trucY.x = 1.0f + dau*vectoNhin->x*vectoNhin->x*a; 
   trucY.z = dau*b;
   trucY.y = -dau*vectoNhin->x;

   Vecto trucX;
   trucX.x = b;
   trucX.z = dau + vectoNhin->z*vectoNhin->z*a;
   trucX.y = -vectoNhin->z; 
   */
   // ---- vectơ nhìn làm trục z địa phương
   donViHoa( vectoNhin );
   Vecto trucX_diaPhuong;
   Vecto trucY_diaPhuong;
   
   if(  (vectoNhin->y != 1.0f) && ((vectoNhin->x != 0.0f) || (vectoNhin->z != 0.0f)) ) {
      // ---- hướng vô tích vectơ nhìn với vectơ trục y cho tính trúc x đia phuong
      Vecto trucY;
      trucY.x = 0.0f;
      trucY.y = 1.0f;
      trucY.z = 0.0f;
      trucX_diaPhuong = tichCoHuong( &trucY, vectoNhin );
      // ---- hướng vô tích vectơ nhìn với trục vô tích
      trucY_diaPhuong = tichCoHuong( vectoNhin, &trucX_diaPhuong );
   }
   else if( (vectoNhin->x == 0.0f) && (vectoNhin->y = -1.0f) && (vectoNhin->z == 0.0f) ) {
      trucX_diaPhuong.x = -1.0f;
      trucX_diaPhuong.y = 0.0f;
      trucX_diaPhuong.z = 0.0f;
   
      trucY_diaPhuong.x = 0.0f;
      trucY_diaPhuong.y = 0.0f;
      trucY_diaPhuong.z = -1.0f;
   }
   else {
      trucX_diaPhuong.x = 1.0f;
      trucX_diaPhuong.y = 0.0f;
      trucX_diaPhuong.z = 0.0f;
      
      trucY_diaPhuong.x = 0.0f;
      trucY_diaPhuong.y = 0.0f;
      trucY_diaPhuong.z = 1.0f;
   }

   // ---- giỡ kết qủa
   maTran[0] = trucX_diaPhuong.x;
   maTran[1] = trucX_diaPhuong.y;
   maTran[2] = trucX_diaPhuong.z;
   maTran[3] = 0.0f;
   
   maTran[4] = trucY_diaPhuong.x;
   maTran[5] = trucY_diaPhuong.y;
   maTran[6] = trucY_diaPhuong.z;
   maTran[7] = 0.0f;
   
   maTran[8] = vectoNhin->x;
   maTran[9] = vectoNhin->y;
   maTran[10] = vectoNhin->z;
   maTran[11] = 0.0f;
   
   maTran[12] = 0.0f;
   maTran[13] = 0.0f;
   maTran[14] = 0.0f;
   maTran[15] = 1.0f;
}


#pragma mark ---- Đăt Biến Hóa Và Gồm Biến Hóa
void datBienHoaChoVat( VatThe *vatThe, Vecto *phongTo, Quaternion *xoay, Vecto *dich ) {
   // ---- phóng to
   datPhongTo( vatThe->phongTo, phongTo->x, phongTo->y, phongTo->z );
   datPhongTo( vatThe->nghichPhongTo, 1.0f/phongTo->x, 1.0f/phongTo->y, 1.0f/phongTo->z );
   // ---- chưa làm xoay
   vatThe->quaternion.w = xoay->w;
   vatThe->quaternion.x = xoay->x;
   vatThe->quaternion.y = xoay->y;
   vatThe->quaternion.z = xoay->z;

   quaternionSangMaTran( xoay, vatThe->xoay );
   quaternionSangMaTran( xoay, vatThe->nghichXoay );   //  nên chép này, không tính hai lần
   latMaTran4x4( vatThe->nghichXoay );
   // ---- dịch
   datDich( vatThe->dich, dich->x, dich->y, dich->z );
   datDich( vatThe->nghichDich, -dich->x, -dich->y, -dich->z );
   gomBienHoaChoVat( vatThe );
}


void gomBienHoaChoVat( VatThe *vatThe ) {
   
   // ==== biến hóa   // kèm thêm ở đang sau như này: [vectơ] * [phóng to] * [xoay] * [dịch]
   // ---- phóng to và xoay
   float maTranKetQua[16];
   nhanMaTranVoiMaTran( maTranKetQua, vatThe->phongTo, vatThe->xoay );
   // ---- phóng to và xoay và dịch
   nhanMaTranVoiMaTran( vatThe->bienHoa, maTranKetQua, vatThe->dich );
   
   // ---- nghịch biến hóa     // kèm nghịch: [vectơ] * [dịch]-1 *[xoay]-1 * [phóng to]-1
   nhanMaTranVoiMaTran( maTranKetQua, vatThe->nghichXoay, vatThe->nghichPhongTo );
   nhanMaTranVoiMaTran( vatThe->nghichBienHoa, vatThe->nghichDich, maTranKetQua );
   // ---- đặc biệt cho nghịch biến hóa cho pháp tuyến
   nhanMaTranVoiMaTran( vatThe->nghichBienHoaChoPhapTuyen, vatThe->nghichPhongTo, vatThe->xoay );
   
   /*   printf( "\n-----vatThe->nghichXoay\n" );
    printf( "%5.3f %5.3f %5.3f %5.3f\n", vatThe->nghichXoay[0], vatThe->nghichXoay[1], vatThe->nghichXoay[2], vatThe->nghichXoay[3] );
    printf( "%5.3f %5.3f %5.3f %5.3f\n", vatThe->nghichXoay[4], vatThe->nghichXoay[5], vatThe->nghichXoay[6], vatThe->nghichXoay[7] );
    printf( "%5.3f %5.3f %5.3f %5.3f\n", vatThe->nghichXoay[8], vatThe->nghichXoay[9], vatThe->nghichXoay[10], vatThe->nghichXoay[11] );
    printf( "%5.3f %5.3f %5.3f %5.3f\n", vatThe->nghichXoay[12], vatThe->nghichXoay[13], vatThe->nghichXoay[14], vatThe->nghichXoay[15] );
    printf( "\n-----vatThe->nghichBienHoa\n" );
    printf( "%5.3f %5.3f %5.3f %5.3f\n", vatThe->nghichBienHoa[0], vatThe->nghichBienHoa[1], vatThe->nghichBienHoa[2], vatThe->nghichBienHoa[3] );
    printf( "%5.3f %5.3f %5.3f %5.3f\n", vatThe->nghichBienHoa[4], vatThe->nghichBienHoa[5], vatThe->nghichBienHoa[6], vatThe->nghichBienHoa[7] );
    printf( "%5.3f %5.3f %5.3f %5.3f\n", vatThe->nghichBienHoa[8], vatThe->nghichBienHoa[9], vatThe->nghichBienHoa[10], vatThe->nghichBienHoa[11] );
    printf( "%5.3f %5.3f %5.3f %5.3f\n", vatThe->nghichBienHoa[12], vatThe->nghichBienHoa[13], vatThe->nghichBienHoa[14], vatThe->nghichBienHoa[15] ); */
}

Vecto xoayVectoQuanhTruc( Vecto *vecto, Vecto *trucXoay, float gocXoay ) {
   
   // ---- tạo quaternion cho biến hóa
   Quaternion quaternionXoay = datQuaternionTuVectoVaGocQuay( trucXoay, gocXoay );
 //  printf( "   -- xoayVectoQuanhTruc: gocXoay %5.3f\n     vecto %5.3f %5.3f %5.3f\n     trucXoay %5.3f %5.3f %5.3f\n",
 //         gocXoay, vecto->x, vecto->y, vecto->z, trucXoay->x, trucXoay->y, trucXoay->z );
   float maTran[16];
   quaternionSangMaTran( &quaternionXoay, maTran );
   
   return nhanVectoVoiMaTran3x3( vecto, maTran );
}


Vecto viTriVatThe( VatThe *vatThe ) {
   
   Vecto viTri;
   viTri.x = vatThe->dich[12];
   viTri.y = vatThe->dich[13];
   viTri.z = vatThe->dich[14];
   
   return viTri;
}


//#pragma mark ---- Xem Điểm
/* Quaternion tuDiemGocXemDiem( Vecto *diemGoc, Vecto *diemXem, float gocXoay ) {
 
 // ---- tính hướng nhìn
 Vecto huongNhin;
 huongNhin.x = diemXem->x - diemGoc->x;
 huongNhin.y = diemXem->y - diemGoc->y;
 huongNhin.z = diemXem->z - diemGoc->z;
 donViHoa( &huongNhin );
 // ---- tính quaternion
 Quaternion quaternion = datQuaternionTuVectoVaGocQuay( &huongNhin, gocXoay );
 
 return quaternion;
 } */

#pragma mark ---- Di Chuyển
// số bức ảnh - số bức ảnh đang tính vị trí (tư đối với số bức ành đầu chu kỳ)
// số bức ảnh chu kỳ - số lượng chu kỳ cho một chu kỳ di chuyển
/*Vecto vanTocSin( Vecto *huong, float bienDo, float soBucAnh, float soBucAnhChuKy ) {
 // ---- tính góc xoay
 float gocXoay = 6.28318f*soBucAnh/soBucAnhChuKy;
 float doLon = bienDo*sinf( gocXoay );
 printf( "gocXoay %5.3f   doLon %5.3f   soBucAnh %5.3f\n", gocXoay, doLon, soBucAnh );
 // ---- vectơ di chuyển từ điểm gốc
 Vecto vanTocDiChuyen;
 vanTocDiChuyen.x = doLon*huong->x;
 vanTocDiChuyen.y = doLon*huong->y;
 vanTocDiChuyen.z = doLon*huong->z;
 
 return vanTocDiChuyen;
 }*/

Vecto viTriSin( Vecto *viTriDau, Vecto *huong, float bienDo, float soBucAnh, float soBucAnhChuKy, float *tocDo ) {
   // ---- tính góc xoay
   float gocXoay = 6.28318f*soBucAnh/soBucAnhChuKy;
   float doLon = bienDo*(1.0f - cosf( gocXoay ));
   *tocDo = bienDo*6.28318f/soBucAnhChuKy*sinf( gocXoay );
   //   printf( "gocXoay %5.3f   doLon %5.3f   soBucAnh %5.3f\n", gocXoay, doLon, soBucAnh );
   // ---- vectơ di chuyển từ điểm gốc
   Vecto viTri;
   viTri.x = viTriDau->x + doLon*huong->x;
   viTri.y = viTriDau->y + doLon*huong->y;
   viTri.z = viTriDau->z + doLon*huong->z;
   
   return viTri;
}

Vecto viTriSin2( Vecto *viTriDau, Vecto *huong, float bienDo, float soBucAnh, float soBucAnhChuKy, float *tocDo ) {
   // ---- tính góc xoay
   float gocXoay = 6.28318f*soBucAnh/soBucAnhChuKy;
   float doLon = bienDo*sinf( gocXoay );
   *tocDo = bienDo*6.28318f/soBucAnhChuKy*cosf( gocXoay );
   //   printf( "gocXoay %5.3f   doLon %5.3f   soBucAnh %5.3f\n", gocXoay, doLon, soBucAnh );
   // ---- vectơ di chuyển từ điểm gốc
   Vecto viTri;
   viTri.x = viTriDau->x + doLon*huong->x;
   viTri.y = viTriDau->y + doLon*huong->y;
   viTri.z = viTriDau->z + doLon*huong->z;
   
   return viTri;
}
